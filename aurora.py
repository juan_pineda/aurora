import gc
import re
import os
import sys
import pdb
import math
import time
import pynbody
import warnings
import numpy as np
import ConfigParser
import scipy.fftpack
from scipy import ndimage
from astropy.io import fits
import multiprocessing as mp
from scipy.stats import norm
import matplotlib.pyplot as plt
from astropy import units as unit
from sklearn.neighbors import KDTree
from astropy import constants as const
from astropy.cosmology import Planck13 as cosmo
#from scipy.spatial import kdtree

warnings.filterwarnings("ignore")


def update_progress(progress):
    barLength = 30  # Modify this to change the length of the progress bar
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = " / Halt\r\n"
    if progress >= 1:
        progress = 1
        status = " / Done\r\n"
    block = int(round(barLength * progress))
    text = "\r// Progress: [{0}] {1}% {2}".format(
        "=" * block + "-" * (barLength - block), round(progress * 100), status)
    sys.stdout.write(text)
    sys.stdout.flush()


class photom_args():
    pass


class spectrom_args():
    pass


class constants_obj():

    def __init__(self):
        # Halpha wavelength in rest frame [cm]
        self.Halpha0 = 6562.78e-8 * unit.cm
        # NII wavelength in rest frame [cm]
        self.NII0 = 6584.00e-8 * unit.cm
        # Effective Halpha recombination rate
        self.eff_recomb_rate_halpha = 1.16e-16 / unit.s
        # Hydrogen cosmological fraction
        self.Xh = 0.76
        # Solar mass
        self.M_sun = const.M_sun.to('g')
        # Myear [s]
        self.myr = 1.0E6 * 365.0 * 24.0 * 3600.0 * unit.s
        # Solar O/H ratio
        self.oh_solar = 8.66
        # Solar metal fraction
        self.z_solar = 0.0126
        # Obscuration coefficient for important emission lines
        # Computed with RV = 3.1 (MW)
        # See Boselli 2012
        self.kMW_OII = 4.751
        self.kMW_Hd = 4.418
        self.kMW_Hg = 4.154
        self.kMW_OIII_4363 = 4.128
        self.kMW_Hb = 3.588
        self.kMW_OIII_4959 = 3.497
        self.kMW_OIII_5007 = 3.452
        self.kMW_OI = 2.642
        self.kMW_NII_6548 = 2.524
        self.kMW_Ha = 2.517
        self.kMW_NII_6584 = 2.507
        self.kMW_SII_6716 = 2.444
        self.kMW_SII_6731 = 2.437
        self.kMW_Pb = 0.832
        self.kMW_Pa = 0.451
        # Hydrogen recombination rate
        self.alphaH = 2.59e-13 * np.power(unit.cm, 3) / unit.s
        # Physical constants defined in CGS system
        self.h = const.h.to('erg s')
        self.c = const.c.to('cm/s')
        self.m_p = const.m_p.to('g')
        self.k_B = const.k_B.to('erg/K')
        self.kpc = const.kpc.to('cm')
        self.G = const.G.to('cm3/(g s2)')


class geometry_obj():

    def __init__(self):
        self.redshift = 0
        self.theta = 0 * unit.deg
        self.phi = 0 * unit.deg
        self.pixsize = 0 * unit.pc
        self.barycenter = False
        self.centerx = 0 * unit.kpc
        self.centery = 0 * unit.kpc
        self.centerz = 0 * unit.kpc
        self.los = ''
        self.dl = 0 * unit.kpc
        self.amin = 0 * unit.kpc
        self.amax = 0 * unit.kpc
        self.bmin = 0 * unit.kpc
        self.bmax = 0 * unit.kpc
        self.cmin = 0 * unit.kpc
        self.cmax = 0 * unit.kpc
        self.focal_length = 0 * unit.kpc
        self.output_dir = ''

    def parse_input(self, ConfigFile):
        geom_config = ConfigParser.SafeConfigParser({
            'redshift': 1.30,
            'theta': 0.,
            'phi': 0.,
            'pixsize': 0.0,
            'barycenter': 'false',
            'centerx': '0',
            'centery': '0',
            'centerz': '0',
            'los': 'x',
            'xmin': '-999.',
            'xmax': '999.',
            'ymin': '-999.',
            'ymax': '999.',
            'zmin': '-999.',
            'zmax': '999.',
            'focal_length': '0.',
            'custom_dir': ''
        }, allow_no_value=True)
        geom_config.read(ConfigFile)
        self.redshift = geom_config.getfloat('geometry', 'redshift')
        self.theta = geom_config.getfloat(
            'geometry', 'theta') * self.theta.unit
        self.phi = geom_config.getfloat('geometry', 'phi') * self.theta.unit
        self.pixsize = geom_config.getfloat(
            'geometry', 'pixsize') * self.pixsize.unit
        self.barycenter = geom_config.getboolean('geometry', 'barycenter')
        self.centerx = geom_config.getfloat(
            'geometry', 'centerx') * self.centerx.unit
        self.centery = geom_config.getfloat(
            'geometry', 'centery') * self.centery.unit
        self.centerz = geom_config.getfloat(
            'geometry', 'centerz') * self.centerz.unit
        list_los = geom_config.get('geometry', 'los')
        self.los = list(list_los)
        self.amin = geom_config.getfloat('geometry', 'xmin') * self.amin.unit
        self.amax = geom_config.getfloat('geometry', 'xmax') * self.amax.unit
        self.bmin = geom_config.getfloat('geometry', 'ymin') * self.bmin.unit
        self.bmax = geom_config.getfloat('geometry', 'ymax') * self.bmax.unit
        self.cmin = geom_config.getfloat('geometry', 'zmin') * self.cmin.unit
        self.cmax = geom_config.getfloat('geometry', 'zmax') * self.cmax.unit
        self.focal_length = geom_config.getfloat(
            'geometry', 'focal_length') * self.focal_length.unit
        self.output_dir = geom_config.get('geometry', 'custom_dir')


class run_obj():

    def __init__(self):
        self.list_input_file = ''
        self.nfft = 0
        self.fft_hsml_min = 0 * unit.pc
        self.fft_hsml_limits = 0 * unit.pc
        self.kernel_scale = 1.0
        self.nvector = 0
        self.gas_minmax_keys = ''
        self.gas_min_values = 0
        self.gas_max_values = 0
        self.star_minmax_keys = ''
        self.star_min_values = 0
        self.star_max_values = 0
        self.dm_minmax_keys = ''
        self.dm_min_values = 0
        self.dm_max_values = 0
        self.overwrite = False
        self.ncpu = 1
        self.script_file = ''

    def parse_input(self, ConfigFile):
        run_config = ConfigParser.SafeConfigParser({
            'input_file': '',
            'fft_hsml_min': '8E-3',
            'nfft': '9',
            'kernel_scale': '1.0',
            'nvector': '4096',
            'nvcpu': '1',
            'gas_minmax_keys': '',
            'gas_min_values': '',
            'gas_max_values': '',
            'star_minmax_keys': '',
            'star_min_values': '',
            'star_max_values': '',
            'dm_minmax_keys': '',
            'dm_min_values': '',
            'dm_max_values': '',
            'script_file': '',
            'overwrite': 'False'
        }, allow_no_value=True)
        run_config.read(ConfigFile)
        run = run_obj()

        list_input_file = run_config.get('run', 'input_file')
        self.input_file = re.split(',|;', ''.join(list_input_file.split()))
        self.fft_hsml_min = run_config.getfloat(
            'run', 'fft_hsml_min') * self.fft_hsml_min.unit
        self.kernel_scale = run_config.getfloat('run', 'kernel_scale')
        self.nfft = run_config.getint('run', 'nfft')
        self.nvector = run_config.getint('run', 'nvector')
        self.ncpu = run_config.getint('run', 'ncpu')
        self.overwrite = run_config.getboolean('run', 'overwrite')

        list_gas_minmax_keys = run_config.get('run', 'gas_minmax_keys')
        list_gas_min_values = run_config.get('run', 'gas_min_values')
        list_gas_max_values = run_config.get('run', 'gas_max_values')

        list_star_minmax_keys = run_config.get('run', 'star_minmax_keys')
        list_star_min_values = run_config.get('run', 'star_min_values')
        list_star_max_values = run_config.get('run', 'star_max_values')

        list_dm_minmax_keys = run_config.get('run', 'dm_minmax_keys')
        list_dm_min_values = run_config.get('run', 'dm_min_values')
        list_dm_max_values = run_config.get('run', 'dm_max_values')
        self.script_file = run_config.get('run', 'script_file')

        if(list_gas_minmax_keys != ''):
            self.gas_minmax_keys = re.split(
                ',|;', ''.join(list_gas_minmax_keys.split()))
        if(list_gas_min_values != ''):
            self.gas_min_values = (np.array(
                re.split(',|;', ''.join(list_gas_min_values.split())))).astype(np.float)
            if(len(self.gas_minmax_keys) != len(self.gas_min_values)):
                print 'The number of elements in gas_minmax_keys and gas_min_values should be equal'
                sys.exit()
        if(list_gas_max_values != ''):
            self.gas_max_values = (np.array(
                re.split(',|;', ''.join(list_gas_max_values.split())))).astype(np.float)
            if(len(self.gas_minmax_keys) != len(self.gas_max_values)):
                print 'The number of elements in gas_minmax_keys and gas_max_values should be equal'
                sys.exit()

        if(list_star_minmax_keys != ''):
            self.star_minmax_keys = re.split(
                ',|;', ''.join(list_star_minmax_keys.split()))
        if(list_star_min_values != ''):
            self.star_min_values = (np.array(
                re.split(',|;', ''.join(list_star_min_values.split())))).astype(np.float)
            if(len(self.stars_minmax_keys) != len(self.stars_min_values)):
                print 'The number of elements in star_minmax_keys and star_min_values should be equal'
                sys.exit()
        if(list_star_max_values != ''):
            self.star_max_values = (np.array(
                re.split(',|;', ''.join(list_star_max_values.split())))).astype(np.float)
            if(len(self.star_minmax_keys) != len(self.star_max_values)):
                print 'The number of elements in star_minmax_keys and star_max_values should be equal'
                sys.exit()

        if(list_dm_minmax_keys != ''):
            self.dm_minmax_keys = re.split(
                ',|;', ''.join(list_dm_minmax_keys.split()))
        if(list_dm_min_values != ''):
            self.dm_min_values = (np.array(
                re.split(',|;', ''.join(list_dm_min_values.split())))).astype(np.float)
            if(len(self.dms_minmax_keys) != len(self.dms_min_values)):
                print 'The number of elements in dm_minmax_keys and dm_min_values should be equal'
                sys.exit()
        if(list_dm_max_values != ''):
            self.dm_max_values = (np.array(
                re.split(',|;', ''.join(list_dm_max_values.split())))).astype(np.float)
            if(len(self.dm_minmax_keys) != len(self.dm_max_values)):
                print 'The number of elements in dm_minmax_keys and dm_max_values should be equal'
                sys.exit()


class spectrom_obj():

    def __init__(self):
        self.presets = ''
        self.sigma_cont = 0.0
        self.spatial_sampl = 0. * unit.arcsec
        self.spectral_sampl = 0. * unit.angstrom
        self.spatial_res = 0. * unit.arcsec
        self.spectral_res = 0.
        self.spatial_dim = 0
        self.spectral_dim = 0
        self.sigma_cont = 0.
        self.sed_file = ''
        self.sed_reference_mass = 0.
        self.ebv = 0.
        self.rv = 3.1
        self.T_HII = 0.

    def parse_input(self, ConfigFile):
        spectrom_config = ConfigParser.SafeConfigParser(allow_no_value=True)
        spectrom_config.read(ConfigFile)

        self.presets = spectrom_config.get('spectrom', 'presets')
        self.sigma_cont = spectrom_config.getfloat('spectrom', 'sigma_cont')

        if self.presets == "sinfoni":
            spectrom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.125',
                'spectral_sampl': '1.95',
                'spatial_res': '0.65',
                'spectral_res': '2500',
                'spatial_dim': '38',
                'spectral_dim': '48',
                'target_snr': '0'
            }, allow_no_value=True)
        elif self.presets == "sinfoni-ao":
            spectrom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.05',
                'spectral_sampl': '1.95',
                'spatial_res': '0.20',
                'spectral_res': '2500',
                'spatial_dim': '64',
                'spectral_dim': '48',
                'target_snr': '0'
            }, allow_no_value=True)
        elif self.presets == "eagle":
            spectrom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.0375',
                'spectral_sampl': '2.625',
                'spatial_res': '0.0975',
                'spectral_res': '4000',
                'spatial_dim': '96',
                'spectral_dim': '64',
                'target_snr': '0'
            }, allow_no_value=True)
        elif self.presets == "kmos":
            spectrom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.20',
                'spectral_sampl': '5.375',
                'spatial_res': '0.70',
                'spectral_res': '1800',
                'spatial_dim': '14',
                'spectral_dim': '64',
                'target_snr': '0'
            }, allow_no_value=True)
        elif self.presets == "muse-wide":
            spectrom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.20',
                'spectral_sampl': '1.95',
                'spatial_res': '0.65',
                'spectral_res': '300',
                'spatial_dim': '44',
                'spectral_dim': '64',
                'target_snr': '0'
            }, allow_no_value=True)
        elif self.presets == "muse-narrow":
            spectrom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.025',
                'spectral_sampl': '1.95',
                'spatial_res': '0.04',
                'spectral_res': '3000',
                'spatial_dim': '44',
                'spectral_dim': '64',
                'target_snr': '0'
            }, allow_no_value=True)
        elif self.presets == "ghasp-instr":
            spectrom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.68',
                'spectral_sampl': '0.30',
                'spatial_res': '2.0',
                'spectral_res': '10000',
                'spatial_dim': '512',
                'spectral_dim': '38',
                'target_snr': '0'
            }, allow_no_value=True)
        else:
            self.presets = "sinfoni"
            spectrom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.125',
                'spectral_sampl': '1.95',
                'spatial_res': '0.65',
                'spectral_res': '2500',
                'spatial_dim': '38',
                'spectral_dim': '48',
                'target_snr': '0'
            }, allow_no_value=True)

        print "// Using presets: ", self.presets
        spectrom_config.read(ConfigFile)
        try:
            self.spatial_sampl = spectrom_config.getfloat(
                'spectrom', 'spatial_sampl') * unit.arcsec
        except:
            pass
        try:
            self.spectral_sampl = spectrom_config.getfloat(
                'spectrom', 'spectral_sampl') * unit.angstrom
        except:
            pass
        try:
            self.spatial_res = spectrom_config.getfloat(
                'spectrom', 'spatial_res') * unit.arcsec
        except:
            pass
        try:
            self.spectral_res = spectrom_config.getfloat(
                'spectrom', 'spectral_res')
        except:
            pass
        try:
            self.spatial_dim = spectrom_config.getint(
                'spectrom', 'spatial_dim')
        except:
            pass
        try:
            self.spectral_dim = spectrom_config.getint(
                'spectrom', 'spectral_dim')
        except:
            pass
        try:
            self.sigma_cont = spectrom_config.getfloat(
                'spectrom', 'sigma_cont')
        except:
            pass
        try:
            self.sed_file = spectrom_config.get('spectrom', 'sed_file')
        except:
            pass
        try:
            self.sed_reference_mass = spectrom_config.getfloat(
                'spectrom', 'sed_reference_mass')
        except:
            pass
        try:
            self.ebv = spectrom_config.getfloat('spectrom', 'ebv')
        except:
            pass
        try:
            self.rv = spectrom_config.getfloat('spectrom', 'rv')
        except:
            pass
        try:
            self.cloud_mass = spectrom_config.getfloat(
                'spectrom', 'cloud_mass') * unit.solMass
        except:
            pass
        try:
            self.cloud_rad = spectrom_config.getfloat(
                'spectrom', 'cloud_rad') * unit.pc
        except:
            pass
        try:
            self.dust_to_gas = spectrom_config.getfloat(
                'spectrom', 'dust_to_gas')
        except:
            pass
        try:
            self.T_HII = spectrom_config.getfloat('spectrom', 'T_HII')
        except:
            pass


class photom_obj():

    def __init__(self):
        self.presets = ''
        self.sed_file = ''
        self.filter_file = ''
        self.sed_reference_mass = ''
        self.sed_dt = 0.
        self.ebv = 0.0
        self.rv = 0.0
        self.spatial_sampl = 0. * unit.arcsec
        self.spatial_res = 0. * unit.arcsec
        self.nx = 0
        self.ny = 0
        self.mag_ab_limits = 0.
        self.dust_to_gas = 0.
        self.sed_model_lambda = 0.
        self.sed_model_flux = 0.
        self.sed_model_obscuration = 0.
        self.boost_ob = 1.0

    def parse_input(self, ConfigFile):
        photom_config = ConfigParser.SafeConfigParser(allow_no_value=True)
        photom_config.read(ConfigFile)

        self.presets = photom_config.get('photom', 'presets')
        self.sed_file = photom_config.get('photom', 'sed_file')
        list_filters = photom_config.get('photom', 'filter_file')
        self.sed_reference_mass = photom_config.getfloat(
            'photom', 'sed_reference_mass')
        self.sed_dt = photom_config.getfloat('photom', 'sed_dt')
        self.ebv = photom_config.getfloat('photom', 'ebv')
        self.rv = photom_config.getfloat('photom', 'rv')
        self.dust_to_gas = photom_config.getfloat('photom', 'dust_to_gas')
        self.boost_ob = photom_config.getfloat('photom', 'boost_ob')
        self.filter_files = re.split(',|;', ''.join(list_filters.split()))

        # Pre-defined settings for photometry mock observations
        if self.presets == "cfht_mega_i":
            photom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.205',
                'spatial_res': '0.80',
                'nx': '30',
                'ny': '30',
                'mag_ab_limit': '25.'
            }, allow_no_value=True)
        elif self.presets == "hst_wfc3_f160w":
            photom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.13',
                'spatial_res': '0.30',
                'nx': '47',
                'ny': '47',
                'mag_ab_limit': '25.25'
            }, allow_no_value=True)
        elif self.presets == "hst_acs_f814w":
            photom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.05',
                'spatial_res': '0.14',
                'nx': '123',
                'ny': '123',
                'mag_ab_limit': '25.5'
            }, allow_no_value=True)
        elif self.presets == "sdss_u":
            photom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.396',
                'spatial_res': '1.40',
                'nx': '512',
                'ny': '512',
                'mag_ab_limit': '22.0'
            }, allow_no_value=True)
        elif self.presets == "sdss_g":
            photom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.396',
                'spatial_res': '1.40',
                'nx': '512',
                'ny': '512',
                'mag_ab_limit': '22.2'
            }, allow_no_value=True)
        elif self.presets == "sdss_r":
            photom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.396',
                'spatial_res': '1.40',
                'nx': '512',
                'ny': '512',
                'mag_ab_limit': '22.2'
            }, allow_no_value=True)
        elif self.presets == "sdss_i":
            photom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.396',
                'spatial_res': '1.40',
                'nx': '512',
                'ny': '512',
                'mag_ab_limit': '21.3'
            }, allow_no_value=True)
        elif self.presets == "sdss_z":
            photom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.396',
                'spatial_res': '1.40',
                'nx': '512',
                'ny': '512',
                'mag_ab_limit': '20.5'
            }, allow_no_value=True)

        else:
            self.presets == "cfht_mega_i"
            photom_config = ConfigParser.SafeConfigParser({
                'spatial_sampl': '0.205',
                'spatial_res': '0.65',
                'nx': '30',
                'ny': '30',
                'mag_ab_limit': '24.92'
            }, allow_no_value=True)

        print "// Using photom presets: ", self.presets

        photom_config.read(ConfigFile)
        self.spatial_sampl = photom_config.getfloat('photom', 'spatial_sampl')*self.spatial_sampl.unit
        self.spatial_res = photom_config.getfloat('photom', 'spatial_res')*self.spatial_res.unit
        self.nx = photom_config.getint('photom', 'nx')
        self.ny = photom_config.getint('photom', 'ny')
        list_mag_ab_limit = photom_config.get('photom', 'mag_ab_limit')
        self.mag_ab_limits = re.split(
            ',|;', ''.join(list_mag_ab_limit.split()))
        self.cloud_mass = photom_config.getfloat(
            'photom', 'cloud_mass') * unit.solMass
        self.cloud_rad = photom_config.getfloat(
            'photom', 'cloud_rad') * unit.pc

        if((len(self.mag_ab_limits) != len(self.filter_files)) & (len(self.mag_ab_limits) > 1)):
            print '// You should provide as many [mag_ab_limit] as [filter_file]'
            return


class maps_obj():

    def __init__(self):
        self.spatial_sampl = 0 * unit.arcsec
        self.nx = 0
        self.ny = 0
        self.gas_keys = ''
        self.star_keys = ''
        self.dm_keys = ''
        self.gas_methods = ''
        self.star_methods = ''
        self.dm_methods = ''
        self.gas_method_keys = ''
        self.star_method_keys = ''
        self.dm_method_keys = ''
        self.gas_keys_bool = 0
        self.star_keys_bool = 0
        self.dm_keys_bool = 0

    def parse_input(self, ConfigFile):
        # Pre-defined settings for maps module
        maps_config = ConfigParser.SafeConfigParser({
            'spatial_sampl': '0.396',
                                                    'nx': '512',
                                                    'ny': '512',
                                                    'gas_keys': 'rho',
                                                    'star_keys': '',
                                                    'dm_keys': '',
                                                    'gas_methods': 'mean',
                                                    'star_methods': '',
                                                    'dm_methods': '',
                                                    'gas_method_keys': 'mass',
                                                    'star_method_keys': '',
                                                    'dm_method_keys': ''
                                                    }, allow_no_value=True)
        # Reading the configuration file
        maps_config.read(ConfigFile)
        # Assigning values
        self.spatial_sampl = maps_config.getfloat(
            'maps', 'spatial_sampl') * self.spatial_sampl.unit
        self.nx = maps_config.getint('maps', 'nx')
        self.ny = maps_config.getint('maps', 'ny')
        # List of keys to use
        list_keys_gas = maps_config.get('maps', 'gas_keys')
        list_keys_star = maps_config.get('maps', 'star_keys')
        list_keys_dm = maps_config.get('maps', 'dm_keys')
        # List of method to generate the maps
        list_methods_gas = maps_config.get('maps', 'gas_methods')
        list_methods_star = maps_config.get('maps', 'star_methods')
        list_methods_dm = maps_config.get('maps', 'dm_methods')
        # List of reference quantities for methods
        list_method_keys_gas = maps_config.get('maps', 'gas_method_keys')
        list_method_keys_star = maps_config.get('maps', 'star_method_keys')
        list_method_keys_dm = maps_config.get('maps', 'dm_method_keys')

        self.gas_keys = re.split(',|;', ''.join(list_keys_gas.split()))
        self.star_keys = re.split(',|;', ''.join(list_keys_star.split()))
        self.dm_keys = re.split(',|;', ''.join(list_keys_dm.split()))

        self.gas_methods = re.split(',|;', ''.join(list_methods_gas.split()))
        self.star_methods = re.split(',|;', ''.join(list_methods_star.split()))
        self.dm_methods = re.split(',|;', ''.join(list_methods_dm.split()))

        self.gas_method_keys = re.split(
            ',|;', ''.join(list_method_keys_gas.split()))
        self.star_method_keys = re.split(
            ',|;', ''.join(list_method_keys_star.split()))
        self.dm_method_keys = re.split(
            ',|;', ''.join(list_method_keys_dm.split()))

        self.gas_keys_bool = np.zeros(len(self.gas_keys)).astype(int)
        self.star_keys_bool = np.zeros(len(self.star_keys)).astype(int)
        self.dm_keys_bool = np.zeros(len(self.dm_keys)).astype(int)

        if((len(self.gas_keys) != len(self.gas_methods))or(len(self.gas_keys) != len(self.gas_method_keys))):
            print '// gas_methods and gas_method_keys should have the same dimension as gas_keys'
            sys.exit()

        for i in range(len(self.gas_keys)):
            try:
                temp = data.gas[self.gas_keys[i]]
                del temp
                self.gas_keys_bool[i] = 1
            except:
                self.gas_keys_bool[i] = 0

        if((len(self.star_keys) != len(self.star_methods))or(len(self.star_keys) != len(self.star_method_keys))):
            print '// star_methods and star_method_keys should have the same dimension as star_keys'
            sys.exit()

        for i in range(len(self.star_keys)):
            try:
                temp = data.star[self.star_keys[i]]
                del temp
                self.star_keys_bool[i] = 1
            except:
                self.star_keys_bool[i] = 0

        if((len(self.dm_keys) != len(self.dm_methods))or(len(self.dm_keys) != len(self.dm_method_keys))):
            print '// dm_methods and dm_method_keys should have the same dimension as dm_keys'
            sys.exit()

        for i in range(len(self.dm_keys)):
            try:
                temp = data.dm[self.dm_keys[i]]
                del temp
                self.dm_keys_bool[i] = 1
            except:
                self.dm_keys_bool[i] = 0


def __cube_convolution(cube, nfft, fft_hsml_limits, pixsize, scale):
    spatial_dim = cube.shape[1]
    spectral_dim = cube.shape[0]
    for i in range(nfft):
        old_max = np.nanmax(cube[:, :, :, i])
        old_min = np.nanmin(cube[:, :, :, i])
        for j in range(spectral_dim):
            map = cube[j, :, :, i]
            # We extend the spatial dimensions with 2 times the seeing to be
            # sure to avoid edge issues.
            extended_borders = math.ceil(spatial_dim / 2.)
            sizex_edge = spatial_dim + 2 * extended_borders
            sizey_edge = spatial_dim + 2 * extended_borders
            extended_map = np.zeros((sizex_edge, sizey_edge))
            extended_map[extended_borders:sizex_edge - extended_borders,
                         extended_borders:sizey_edge - extended_borders] = map
            # Spatial smoothing
            psf_fwhm = scale * fft_hsml_limits[i] / pixsize.to('kpc').value
            psf_sigma = psf_fwhm / (2.0 * math.sqrt(2.0 * math.log(2.0)))
            if((psf_sigma <= 1.0) | (old_max == 0)):
                continue
            # Create a 2D Gaussian modelling the PSF, centrered in the middle
            # of the array
            psfx = np.exp(-0.5 * np.power((np.arange(sizex_edge) - sizex_edge /
                                           2.) / psf_sigma, 2)) / (psf_sigma * math.sqrt(2.0 * math.pi))
            psfy = np.exp(-0.5 * np.power((np.arange(sizey_edge) - sizey_edge /
                                           2.) / psf_sigma, 2)) / (psf_sigma * math.sqrt(2.0 * math.pi))
            psf = np.tile(psfx, (sizex_edge, 1)) * \
                np.transpose(np.tile(psfy, (sizey_edge, 1)))
            psf = np.roll(psf, int(sizex_edge / 2), axis=0)
            psf = np.roll(psf, int(sizex_edge / 2), axis=1)
            fft_psf = np.fft.fft2(psf)
            cube[j, :, :, i] = (np.fft.ifft2(fft_psf * np.fft.fft2(extended_map[:, :]))).real[
                extended_borders:sizex_edge - extended_borders, extended_borders:sizey_edge - extended_borders]

        new_max = np.nanmax(cube[:, :, :, i])
        new_min = np.nanmin(cube[:, :, :, i])
        if((psf_sigma > 1.0) & (old_max > 0)):
            cube[:, :, :, i] = np.multiply(cube[:, :, :, i], old_max / new_max)


def __map_convolution(map, psf_fwhm, nx, ny):
    psf_sigma = psf_fwhm / (2.0 * math.sqrt(2.0 * math.log(2.0)))
    # We extend the spatial dimensions with 2 times the seeing to be sure to
    # avoid edge issues.
    extended_borders = math.ceil(2. * psf_fwhm)
    sizex_edge = nx + 2 * extended_borders
    sizey_edge = ny + 2 * extended_borders
    extended_map = np.zeros((sizey_edge, sizex_edge))
    extended_map[extended_borders:sizex_edge - extended_borders,
                 extended_borders:sizey_edge - extended_borders] = map
    # Create a 2D Gaussian modelling the PSF, centrered in the middle of the
    # array
    psfx = np.exp(-0.5 * np.power((np.arange(sizex_edge) - sizex_edge /
                                   2.) / psf_sigma, 2)) / (psf_sigma * math.sqrt(2.0 * math.pi))
    psfy = np.exp(-0.5 * np.power((np.arange(sizey_edge) - sizey_edge /
                                   2.) / psf_sigma, 2)) / (psf_sigma * math.sqrt(2.0 * math.pi))
    psf = np.tile(psfx, (sizex_edge, 1)) * \
        np.transpose(np.tile(psfy, (sizey_edge, 1)))
    psf = np.roll(psf, int(sizex_edge / 2), axis=0)
    psf = np.roll(psf, int(sizex_edge / 2), axis=1)
    fft_psf = np.fft.fft2(psf)
    map[:, :] = (np.fft.ifft2(fft_psf * np.fft.fft2(extended_map[:, :]))).real[
        extended_borders:sizex_edge - extended_borders, extended_borders:sizey_edge - extended_borders]


def __map_adaptive_convolution(map, nfft, fft_hsml_limits, pixsize, method, scale):
    # We extend the spatial dimensions with 2 times the seeing to be sure to
    # avoid edge issues.
    nx = map.shape[0]
    ny = map.shape[1]
    extended_borders_x = math.ceil(nx / 2.)
    extended_borders_y = math.ceil(ny / 2.)
    sizex_edge = nx + 2 * extended_borders_x
    sizey_edge = ny + 2 * extended_borders_y
    extended_cube = np.zeros((sizex_edge, sizey_edge, nfft))
    if(method == 'min'):
        map = -map
    extended_cube[extended_borders_x:sizex_edge - extended_borders_x,
                  extended_borders_y:sizey_edge - extended_borders_y, :] = map

    for i in range(nfft):
        if(np.min(map[:, :, i]) == np.max(map[:, :, i])):
            continue
        # Spatial smoothing

        if(i == nfft - 1):
            psf_fwhm = (2 * fft_hsml_limits[i - 1] / pixsize)**1.0
        else:
            psf_fwhm = (fft_hsml_limits[i] / pixsize)**1.0

        if(psf_fwhm < 1.0):
            continue

        psf_sigma = scale * psf_fwhm / (2.0 * math.sqrt(2.0 * math.log(2.0)))

        old_min = np.nanmin(map[:, :, i])
        old_max = np.nanmax(map[:, :, i])

        if(old_min == old_max):
            old_min = 0
        extended_cube[:, :, i] -= old_min
        extended_cube[:, :, i] = extended_cube[:, :, i] / (old_max - old_min)
        map[:, :, i] -= old_min
        map[:, :, i] = map[:, :, i] / (old_max - old_min)
        # Create a 2D Gaussian modelling the PSF, centrered in the middle of
        # the array
        psfx = np.exp(-0.5 * np.power((np.arange(sizex_edge) - sizex_edge /
                                       2.) / psf_sigma, 2)) / (psf_sigma * math.sqrt(2.0 * math.pi))
        psfy = np.exp(-0.5 * np.power((np.arange(sizey_edge) - sizey_edge /
                                       2.) / psf_sigma, 2)) / (psf_sigma * math.sqrt(2.0 * math.pi))
        psf = np.tile(psfx, (sizex_edge, 1)) * \
            np.transpose(np.tile(psfy, (sizey_edge, 1)))
        psf = np.transpose(np.tile(psfx, (sizey_edge, 1))) * \
            np.tile(psfy, (sizex_edge, 1))
        psf = np.roll(psf, int(sizex_edge / 2), axis=0)
        psf = np.roll(psf, int(sizey_edge / 2), axis=1)
        fft_psf = np.fft.fft2(psf)
        fft_map = np.fft.fft2(extended_cube[:, :, i])
        map[:, :, i] = (np.fft.ifft2(fft_psf * fft_map)).real[extended_borders_x:sizex_edge -
                                                              extended_borders_x, extended_borders_y:sizey_edge - extended_borders_y]
        map[:, :, i] *= (old_max - old_min)
        map[:, :, i] += old_min
        if(method == 'min'):
            map[:, :, i] = -map[:, :, i]


def __project_quantity_min(geom, run, maps, data_x, data_y, data_z, data_min, data_quantity, data_smooth):

    level = run.fft_hsml_limits
    quantity_map = np.zeros((maps.nx, maps.ny, run.nfft))
    min_map = np.zeros((maps.nx, maps.ny, run.nfft))
    min_map[:, :, :] = np.max(data_min)

    if(geom.focal_length > 0.):
        data_x = data_x * \
            geom.focal_length.to('kpc').value / \
            (data_z + geom.dl.to('kpc').value)
        data_y = data_y * \
            geom.focal_length.to('kpc').value / \
            (data_z + geom.dl.to('kpc').value)

    # We want to use gas cells inside the field of view
    x = (np.floor(((data_x + maps.nx * geom.pixsize.to('kpc').value /
                    2.) / geom.pixsize.to('kpc').value))).astype(int)
    y = (np.floor(((data_y + maps.ny * geom.pixsize.to('kpc').value /
                    2.) / geom.pixsize.to('kpc').value))).astype(int)
    ok = np.where((x >= 0) & (x < maps.nx) & (y >= 0) & (y < maps.ny))

    if(len(ok[0]) == 0):
        return (quantity_map, min_map)

    vect_x = data_x[ok]
    vect_y = data_y[ok]
    vect_min = data_min[ok]
    vect_quantity = data_quantity[ok]
    vect_smooth = data_smooth[ok]
    x = x[ok]
    y = y[ok]

    sorted = np.argsort(vect_min)
    vect_x = vect_x[sorted]
    vect_y = vect_y[sorted]
    vect_quantity = vect_quantity[sorted]
    vect_smooth = data_smooth[sorted]
    vect_min = vect_min[sorted]
    x = x[sorted]
    y = y[sorted]

    for i in range(run.nfft):
        if(i == 0):
            ok_level = np.where((vect_smooth < level[i]) & (
                vect_min < min_map[x, y, i]))
        elif(i == run.nfft - 1):
            ok_level = np.where(
                (vect_smooth > level[i - 1]) & (vect_min < min_map[x, y, i]))
        else:
            ok_level = np.where((vect_smooth < level[i]) & (
                vect_smooth > level[i - 1]) & (vect_min < min_map[x, y, i]))
        if(ok_level[0].size == 0):
            continue

        vect_index = x[ok_level] + maps.ny * y[ok_level]
        junk, unique_ind = np.unique(vect_index, return_index=True)

        min_map[x[ok_level[0][unique_ind]], y[ok_level[0][unique_ind]],
                i] = vect_min[ok_level[0][unique_ind]]
        quantity_map[x[ok_level[0][unique_ind]], y[ok_level[0][
            unique_ind]], i] = vect_quantity[ok_level[0][unique_ind]]

    return (quantity_map, min_map)


def __project_quantity_max(geom, run, maps, data_x, data_y, data_z, data_max, data_quantity, data_smooth):

    level = run.fft_hsml_limits
    max_map = np.zeros((maps.nx, maps.ny, run.nfft))
    quantity_map = np.zeros((maps.nx, maps.ny, run.nfft))
    max_map[:, :, :] = np.min(data_max)

    if(geom.focal_length > 0.):
        data_x = data_x * \
            geom.focal_length.to('kpc').value / \
            (data_z + geom.dl.to('kpc').value)
        data_y = data_y * \
            geom.focal_length.to('kpc').value / \
            (data_z + geom.dl.to('kpc').value)

    # We want to use gas cells inside the field of view
    x = (np.floor(((data_x + maps.nx * geom.pixsize.to('kpc').value /
                    2.) / geom.pixsize.to('kpc').value))).astype(int)
    y = (np.floor(((data_y + maps.ny * geom.pixsize.to('kpc').value /
                    2.) / geom.pixsize.to('kpc').value))).astype(int)
    ok = np.where((x >= 0) & (x < maps.nx) & (y >= 0) & (y < maps.ny))

    if(len(ok[0]) == 0):
        return (quantity_map, max_map)

    vect_x = data_x[ok]
    vect_y = data_y[ok]
    vect_max = data_max[ok]
    vect_quantity = data_quantity[ok]
    vect_smooth = data_smooth[ok]
    x = x[ok]
    y = y[ok]

    sorted = np.argsort((-1) * vect_max)
    vect_x = vect_x[sorted]
    vect_y = vect_y[sorted]
    vect_quantity = vect_quantity[sorted]
    vect_smooth = vect_smooth[sorted]
    vect_max = vect_max[sorted]
    x = x[sorted]
    y = y[sorted]

    for i in range(run.nfft):
        if(i == 0):
            ok_level = np.where((vect_smooth < level[i]) & (
                vect_max > max_map[x, y, i]))
        elif(i == run.nfft - 1):
            ok_level = np.where(
                (vect_smooth > level[i - 1]) & (vect_max > max_map[x, y, i]))
        else:
            ok_level = np.where((vect_smooth < level[i]) & (
                vect_smooth > level[i - 1]) & (vect_max > max_map[x, y, i]))
        if(ok_level[0].size == 0):
            continue

        vect_index = x[ok_level] + maps.ny * y[ok_level]
        junk, unique_ind = np.unique(vect_index, return_index=True)

        max_map[x[ok_level[0][unique_ind]], y[ok_level[0][unique_ind]],
                i] = vect_max[ok_level[0][unique_ind]]
        quantity_map[x[ok_level[0][unique_ind]], y[ok_level[0][
            unique_ind]], i] = vect_quantity[ok_level[0][unique_ind]]

    return (quantity_map, max_map)


def __project_quantity_mean(geom, run, maps, data_x, data_y, data_z, data_mean, data_quantity, data_smooth):

    level = run.fft_hsml_limits
    quantity_map = np.zeros((maps.nx, maps.ny, run.nfft))
    mean_map = np.zeros((maps.nx, maps.ny, run.nfft))
    if(geom.focal_length > 0.):
        data_x = data_x * \
            geom.focal_length.to('kpc').value / \
            (data_z + geom.dl.to('kpc').value)
        data_y = data_y * \
            geom.focal_length.to('kpc').value / \
            (data_z + geom.dl.to('kpc').value)

    temp = np.mean(data_mean)
    temp = np.mean(data_quantity)
    # We want to use gas cells inside the field of view
    x = (np.floor(((data_x + maps.nx * geom.pixsize.to('kpc').value /
                    2.) / geom.pixsize.to('kpc').value))).astype(int)
    y = (np.floor(((data_y + maps.ny * geom.pixsize.to('kpc').value /
                    2.) / geom.pixsize.to('kpc').value))).astype(int)
    ok = np.where((x >= 0) & (x < maps.nx) & (y >= 0) & (y < maps.ny))

    if(len(ok[0]) == 0):
        return (quantity_map, mean_map)

    vect_x = data_x[ok]
    vect_y = data_y[ok]
    vect_mean = data_mean[ok]
    vect_quantity = data_quantity[ok]
    vect_smooth = data_smooth[ok]
    x = x[ok]
    y = y[ok]

    for i in range(run.nfft):
        if(run.nfft > 1):
            if(i == 0):
                ok_level = np.where((vect_smooth < level[i]))
            elif(i == run.nfft - 1):
                ok_level = np.where((vect_smooth > level[i - 1]))
            else:
                ok_level = np.where((vect_smooth < level[i]) & (
                    vect_smooth > level[i - 1]))
        else:
            ok_level = np.where(vect_smooth > 0.0)
        if(ok_level[0].size == 0):
            continue

        H_mean, xedges, yedges = np.histogram2d(x[ok_level], y[ok_level], bins=(
            maps.nx, maps.ny), range=[[0, maps.nx], [0, maps.ny]], weights=vect_mean[ok_level])
        H_quantity, xedges, yedges = np.histogram2d(x[ok_level], y[ok_level], bins=(maps.nx, maps.ny), range=[
                                                    [0, maps.nx], [0, maps.ny]], weights=vect_quantity[ok_level] * vect_mean[ok_level])
        mean_map[:, :, i] += H_mean
        quantity_map[:, :, i] += H_quantity

    return (quantity_map, mean_map)


def __project_quantity_sum(geom, run, maps, data_x, data_y, data_z, data_quantity, data_smooth):

    level = run.fft_hsml_limits
    quantity_map = np.zeros((maps.nx, maps.ny, run.nfft))

    if(geom.focal_length > 0.):
        data_x = data_x * \
            geom.focal_length.to('kpc').value / \
            (data_z + geom.dl.to('kpc').value)
        data_y = data_y * \
            geom.focal_length.to('kpc').value / \
            (data_z + geom.dl.to('kpc').value)

    # We want to use gas cells inside the field of view
    x = (np.floor(((data_x + maps.nx * geom.pixsize.to('kpc').value /
                    2.) / geom.pixsize.to('kpc').value))).astype(int)
    y = (np.floor(((data_y + maps.ny * geom.pixsize.to('kpc').value /
                    2.) / geom.pixsize.to('kpc').value))).astype(int)
    ok = np.where((x >= 0) & (x < maps.nx) & (y >= 0) & (y < maps.ny))

    if(len(ok[0]) == 0):
        return quantity_map

    vect_x = data_x[ok]
    vect_y = data_y[ok]
    vect_quantity = data_quantity[ok]
    vect_smooth = data_smooth[ok]
    x = x[ok]
    y = y[ok]

    for i in range(run.nfft):
        if(i == 0):
            ok_level = np.where((vect_smooth < level[i]))
        elif(i == run.nfft - 1):
            ok_level = np.where((vect_smooth > level[i - 1]))
        else:
            ok_level = np.where((vect_smooth < level[i]) & (
                vect_smooth > level[i - 1]))
        if(ok_level[0].size == 0):
            continue

        H_quantity, xedges, yedges = np.histogram2d(x[ok_level], y[ok_level], bins=(
            maps.nx, maps.ny), range=[[0, maps.nx], [0, maps.ny]], weights=vect_quantity[ok_level])
        quantity_map[:, :, i] += H_quantity

    return quantity_map


def __project_photom_flux(geom, photom, run, args):

    flux_map = np.zeros((photom.nx, photom.ny))
    level = run.fft_hsml_limits

    kpc = const.kpc.to('cm').value
    c = const.c.to('km/s').value

    # Loop over stars
    vect_x = args.data_star_x
    vect_y = args.data_star_y
    vect_z = args.data_star_z
    vect_vz = args.data_star_vz
    vect_mass = args.data_star_mass
    vect_metal = args.data_star_metal
    vect_age = args.data_star_age
    temp = np.mean(vect_age)
    temp = np.mean(vect_mass)
    if(geom.focal_length > 0.):
        vect_x = vect_x * \
            geom.focal_length.to('kpc').value / \
            (vect_z + geom.dl.to('kpc').value)
        vect_y = vect_y * \
            geom.focal_length.to('kpc').value / \
            (vect_z + geom.dl.to('kpc').value)

    # We want to use star cells inside the field of view
    x = (np.floor(((vect_x + photom.nx * geom.pixsize.to('kpc').value /
                    2.) / geom.pixsize.to('kpc').value))).astype(int)
    y = (np.floor(((vect_y + photom.ny * geom.pixsize.to('kpc').value /
                    2.) / geom.pixsize.to('kpc').value))).astype(int)
    ok = np.where((x >= 0) & (x < photom.nx) & (y >= 0) & (y < photom.ny))

    if(len(ok[0]) == 0):
        return flux_map

    nok = ok[0].size
    vect_x = vect_x[ok]
    vect_y = vect_y[ok]
    vect_z = vect_z[ok]
    vect_vz = vect_vz[ok]
    vect_mass = vect_mass[ok]
    vect_metal = vect_metal[ok]
    vect_age = vect_age[ok]
    x = x[ok]
    y = y[ok]

    if(photom.boost_ob > 1.0):
        ob = np.where(vect_age < 10.)
        vect_mass[ob] = vect_mass[ob] * photom.boost_ob

    if(photom.dust_to_gas > 0):
        # Query tree to get gas particles
        vect_r = zip(vect_x, vect_y)
        vect_cd = np.zeros(nok)
        # Loop over levels
        for i in range(run.nfft):
            dx = np.mean(args.data_gas_smooth[i]) * math.sqrt(3.0) / 2.0
            vol_cell = np.mean(args.data_gas_smooth[i])**3
            vol_cloud = 4.0 / 3.0 * math.pi * \
                (photom.cloud_rad.to('kpc').value)**3
            mass_cloud = photom.cloud_mass.to('1e10 solMass').value
            #rho_cloud = 10e3*unit.solMass/vol_cloud
            # Find all the gas element obscuring the line of sight
            gas_column = args.tree_gas[i].query_radius(vect_r, dx)
            # Add the result to a column density array
            for j in range(nok):
                if(len(gas_column[j]) > 1):
                    front = np.where(
                        ((args.data_gas_z[i])[gas_column[j]]) > vect_z[j])
                    if(front[0].size > 0):
                        dr = np.sqrt(np.power(((args.data_gas_x[i])[gas_column[j]])[
                                     front] - vect_x[j], 2.0) + np.power(((args.data_gas_y[i])[gas_column[j]])[front] - vect_y[j], 2.0))
                        fill_fact = (args.data_gas_mass[i][gas_column[j]][
                                     front] / mass_cloud) * (vol_cloud / vol_cell)
                        vect_cd[j] = vect_cd[j] + np.sum(((args.data_gas_rho[i])[gas_column[j]])[front] * fill_fact * (
                            ((args.data_gas_metal[i])[gas_column[j]])[front] / 0.02) / ((dx + dr) * kpc))

        vect_ebv = vect_cd * photom.dust_to_gas / photom.rv
    else:
        vect_ebv = np.tile(photom.ebv, nok)

    for i in range(nok):
        sed_model_lambda = photom.sed_model_lambda * (
            1.0 + geom.redshift + vect_vz[i] / c)
        sed_model_flux = photom.sed_model_flux / \
            (1.0 + geom.redshift + vect_vz[i] / c)
        sed_obscuration = photom.sed_obscuration / \
            (1.0 + geom.redshift + vect_vz[i] / c)
        # Find the closest time in the SED timesteps
        age_index = np.argmin(np.absolute(photom.sed_model_time - vect_age[i]))
        model_closest_age = np.where(
            photom.sed_model_time == photom.sed_model_time[age_index])
        # Numerical integration of the luminosity in the band
        attenuation = np.power(
            10., -0.4 * vect_ebv[i] * sed_obscuration[model_closest_age])
        integrated_lum = (vect_mass[i] / photom.sed_reference_mass) * 1e-16 * np.trapz(sed_model_flux[
            model_closest_age] * attenuation * photom.filter_trans, sed_model_lambda[model_closest_age])
        integrated_flux = integrated_lum * \
            (1.0e32 / (4.0 * math.pi * (geom.dl.value * 1.0E3 * kpc)**2))
        flux_map[y[i], x[i]] += integrated_flux

    return flux_map


def __project_spectrom_flux(geom, spectrom, run, args):

    cube = np.zeros((spectrom.spectral_dim, spectrom.spatial_dim,
                     spectrom.spatial_dim, run.nfft))
    level = run.fft_hsml_limits

    aconst = constants_obj()
    obscuration = aconst.kMW_Ha
    kpc = const.kpc.to('cm').value
    c = const.c.to('cm/s').value
    dl = geom.dl.to('cm').value
    k_B = aconst.k_B.value
    m_p = aconst.m_p.to('g').value
    h = aconst.h.to('1e-16 erg s').value
    Xh = aconst.Xh
    Halpha0 = aconst.Halpha0.to('angstrom').value
    NII0 = aconst.NII0.to('angstrom').value
    Halpha_em = (1.0 + geom.redshift) * Halpha0
    NII_em = (1.0 + geom.redshift) * NII0
    wavelength = np.arange(spectrom.spectral_dim)
    wavelength = (wavelength - spectrom.spectral_dim / 2.) * \
        spectrom.spectral_sampl.to('angstrom').value + Halpha_em

    # Loop over stars
    vect_x = args.data_gas1_x
    vect_y = args.data_gas1_y
    vect_z = args.data_gas1_z
    vect_vz = args.data_gas1_vz
    vect_dens = args.data_gas1_dens
    vect_metal = args.data_gas1_metal
    vect_temp = args.data_gas1_temp
    vect_HII = args.data_gas1_HII
    vect_smooth = args.data_gas1_smooth
    if(geom.focal_length > 0.):
        vect_x = vect_x * \
            geom.focal_length.to('kpc').value / \
            (vect_z + geom.dl.to('kpc').value)
        vect_y = vect_y * \
            geom.focal_length.to('kpc').value / \
            (vect_z + geom.dl.to('kpc').value)

    # We want to use star cells inside the field of view
    x = (np.floor(((vect_x + spectrom.spatial_dim * geom.pixsize.to('kpc').value /
                    2.) / geom.pixsize.to('kpc').value))).astype(int)
    y = (np.floor(((vect_y + spectrom.spatial_dim * geom.pixsize.to('kpc').value /
                    2.) / geom.pixsize.to('kpc').value))).astype(int)
    ok = np.where((x >= 0) & (x < spectrom.spatial_dim) & (
        y >= 0) & (y < spectrom.spatial_dim) & (vect_HII > 0.))
    nok = len(ok[0])

    if(nok == 0):
        return cube

    nok = ok[0].size
    vect_x = vect_x[ok]
    vect_y = vect_y[ok]
    vect_z = vect_z[ok]
    vect_vz = vect_vz[ok]
    vect_dens = vect_dens[ok]
    vect_metal = vect_metal[ok]
    vect_temp = vect_temp[ok]
    vect_HII = vect_HII[ok]
    vect_smooth = vect_smooth[ok]
    x = x[ok]
    y = y[ok]
    vect_index = x + spectrom.spatial_dim * y

    if(spectrom.dust_to_gas > 0):
        # Query tree to get gas particles
        vect_r = zip(vect_x, vect_y)
        vect_cd = np.zeros(nok)
        # Loop over levels
        for i in range(run.nfft):
            dx = np.mean(args.data_gas_smooth[i]) * math.sqrt(3.0) / 2.0
            vol_cell = np.mean(args.data_gas_smooth[i])**3
            vol_cloud = 4.0 / 3.0 * math.pi * \
                (spectrom.cloud_rad.to('kpc').value)**3
            mass_cloud = spectrom.cloud_mass.to('1e10 solMass').value
            #rho_cloud = 10e3*unit.solMass/vol_cloud
            # Find all the gas element obscuring the line of sight
            gas_column = args.tree_gas[i].query_radius(vect_r, dx)
            # Add the result to a column density array
            for j in range(nok):
                if(len(gas_column[j]) > 1):
                    front = np.where(
                        ((args.data_gas_z[i])[gas_column[j]]) > vect_z[j])
                    if(front[0].size > 0):
                        dr = np.sqrt(np.power(((args.data_gas_x[i])[gas_column[j]])[
                                     front] - vect_x[j], 2.0) + np.power(((args.data_gas_y[i])[gas_column[j]])[front] - vect_y[j], 2.0))
                        fill_fact = (args.data_gas_mass[i][gas_column[j]][
                                     front] / mass_cloud) * (vol_cloud / vol_cell)
                        vect_cd[j] = vect_cd[j] + np.sum(((args.data_gas_rho[i])[gas_column[j]])[front] * fill_fact * (
                            ((args.data_gas_metal[i])[gas_column[j]])[front] / 0.02) / ((dx + dr) * kpc))

        vect_ebv = vect_cd * spectrom.dust_to_gas / spectrom.rv
    else:
        vect_ebv = np.tile(spectrom.ebv, nok)

    Halpha_obs = (vect_vz / c) * Halpha_em + Halpha_em
    NII_obs = (vect_vz / c) * NII_em + NII_em
    Halpha_photon_energy = (h * c) / Halpha_obs
    NII_photon_energy = (h * c) / NII_obs
    # Velocity dispersion of the emission line
    Halpha_sigma = (
        np.sqrt(k_B * vect_temp / (m_p * math.pow(c, 2)))) * Halpha_obs
    wrong = np.where(Halpha_sigma < 1e-3)
    if(len(wrong[0]) > 0):
        print vect_temp[wrong]
    NII_sigma = (np.sqrt(k_B * vect_temp / (m_p * math.pow(c, 2)))) * NII_obs
    # We use case-B Hydrogen effective recombination rate coefficient
    # definition found in Osterbrock & Ferland (2006)
    alphaH = aconst.alphaH.to('cm3/s').value * \
        np.power(vect_temp / 1.0e4, -0.845)
    Halpha_lum = np.power(vect_smooth * kpc, 3) * np.power(vect_dens *
                                                           vect_HII * Xh / m_p, 2) * (h * c / Halpha0) * alphaH
    Halpha_flux = Halpha_lum / (4.0 * math.pi * math.pow(dl, 2))
    NII_flux = Halpha_flux * \
        np.exp(((vect_metal - 9.07) / 0.79) * math.log(10.))
    Halpha_flux *= np.power(10, -0.4 * vect_ebv * obscuration)

    for i in range(run.nfft):
        if(i == 0):
            ok_level = np.where(
                (vect_smooth < 1.1 * run.fft_hsml_limits[i]))[0]
        elif(i == run.nfft - 1):
            ok_level = np.where(
                (vect_smooth > 1.1 * run.fft_hsml_limits[i - 1]))[0]
        else:
            ok_level = np.where((vect_smooth < 1.1 * run.fft_hsml_limits[i]) & (
                vect_smooth > 1.1 * run.fft_hsml_limits[i - 1]))[0]
        nok_level = ok_level.size
        if(nok_level == 0):
            continue
        # Find unique indices
        duplicated, unique_ind = np.unique(
            vect_index[ok_level], return_index=True)
        # Emission line array creation
        line = wavelength
        line = np.tile(line, (nok_level, 1))
        Halpha_obs_level = np.transpose(
            np.tile(Halpha_obs[ok_level], (spectrom.spectral_dim, 1)))
        NII_obs_level = np.transpose(
            np.tile(NII_obs[ok_level], (spectrom.spectral_dim, 1)))
        Halpha_sigma_level = np.transpose(
            np.tile(Halpha_sigma[ok_level], (spectrom.spectral_dim, 1)))
        NII_sigma_level = np.transpose(
            np.tile(NII_sigma[ok_level], (spectrom.spectral_dim, 1)))
        Halpha_flux_level = np.transpose(
            np.tile(Halpha_flux[ok_level], (spectrom.spectral_dim, 1)))
        NII_flux_level = np.transpose(
            np.tile(NII_flux[ok_level], (spectrom.spectral_dim, 1)))
        # Emission lines flux in erg.s^-1.cm^-2.microns^-1
        line = Halpha_flux_level / (Halpha_sigma_level * 1e4 * math.sqrt(
            2.0 * math.pi)) * np.exp(-0.5 * ((line - Halpha_obs_level) / Halpha_sigma_level)**2)
        # Computing NII emission line according to Inobe et al. (1990)
        line += NII_flux_level / (NII_sigma_level * 1e4 * math.sqrt(
            2.0 * math.pi)) * np.exp(-0.5 * ((line - NII_obs_level) / NII_sigma_level)**2)
        # Sum all the lines for a given index
        for j in range(duplicated.size):
            to_sum = np.where(vect_index[ok_level] == duplicated[j])[0]
            line[unique_ind[j], :] = np.sum(line[to_sum, :], axis=0)
        # Remove duplicated emission lines
        line = line[unique_ind, :]
        cube[:, y[ok_level[unique_ind]], x[ok_level[unique_ind]], i] = np.add(
            cube[:, y[ok_level[unique_ind]], x[ok_level[unique_ind]], i], np.transpose(line))

    return cube


def __write_fits_map(geometry, maps, run, image, key, method, method_key, type, ifile, ilos):

    current_dir = os.getcwd()
    maps_dir = run.input_file[ifile].split(
        '.')[0] + '_maps_nx' + str(maps.nx) + 'ny' + str(maps.ny)
    try:
        os.mkdir(maps_dir)
    except:
        print '// ' + maps_dir + ' exists'

    os.chdir(maps_dir)

    if(geometry.output_dir == ''):
        if(geometry.redshift < 0.01):
            geometry.output_dir = 'z' + str(int(geometry.redshift * 1000) / 1000.) + '_' + type + '_theta' + str(
                int(geometry.theta.value)) + '_phi' + str(int(geometry.phi.value)) + '_los' + geometry.los[ilos]
        else:
            geometry.output_dir = 'z' + str(int(geometry.redshift * 100) / 100.) + '_' + type + '_theta' + str(
                int(geometry.theta.value)) + '_phi' + str(int(geometry.phi.value)) + '_los' + geometry.los[ilos]

    try:
        os.mkdir(geometry.output_dir)
    except:
        print '// ' + geometry.output_dir + ' exists'

    os.chdir(geometry.output_dir)

    # Writing maps to FITS files
    image_hdu = fits.PrimaryHDU(np.transpose(image))
    image_hdulist = fits.HDUList([image_hdu])
    if(method_key == ''):
        name = key + '_' + method
    else:
        name = key + '_' + method + '_' + method_key
    prihdr = image_hdu.header
    # Filling FITS header
    prihdr['BITPIX'] = -32
    prihdr['NAXIS'] = 3
    prihdr['NAXIS1'] = maps.nx
    prihdr['NAXIS2'] = maps.ny
    prihdr['BSCALE'] = 1.0
    prihdr['BZERO'] = 0
    prihdr['BUNIT'] = ''
    prihdr['CDELT1'] = -maps.spatial_sampl.to('deg').value
    prihdr['CDELT2'] = maps.spatial_sampl.to('deg').value
    prihdr['CDELT1_PC'] = geometry.pixsize.to('pc').value
    prihdr['CDELT2_PC'] = geometry.pixsize.to('pc').value
    prihdr['CRPIX1'] = (maps.nx - 1) / 2.
    prihdr['CRPIX2'] = (maps.ny - 1) / 2.
    prihdr['CRVAL1'] = 0.
    prihdr['CRVAL2'] = 0.
    prihdr['CUNIT1'] = 'DEG'
    prihdr['CUNIT2'] = 'DEG'
    prihdr['CD1_1'] = -maps.spatial_sampl.to('deg').value
    prihdr['CD1_2'] = 0.
    prihdr['CD2_1'] = 0.
    prihdr['CD2_2'] = maps.spatial_sampl.to('deg').value
    prihdr['CTYPE1'] = 'RA---TAN'
    prihdr['CTYPE2'] = 'DEC--TAN'
    prihdr['RA'] = 0.
    prihdr['DEC'] = 0.
    prihdr['RADECSYS'] = 'FK5'
    prihdr['EQUINOX'] = 2000.

    image_hdulist.writeto(name + '.fits', clobber=True)
    os.chdir(current_dir)


def __compute_magnitude_from_flux(flux, filter_lambda, filter_trans):
    # We want to compute the spectral flux density in erg.s^-1.cm^-2.Hz^-1
    # We assume the flux to be constant within the spectral band
    # Thus Fnu = Flux/integral(Trans*dnu)
    # |dnu| = |(c/lambda^2)*dlambda|
    # Assuming a flux in 1E-16 erg.s^-1.cm^-2
    # Integrating the transmission of the filter over its spectral range
    integral_trans = np.trapz(
        filter_trans / np.power(filter_lambda, 2), filter_lambda)
    # Converting light speed from cgs to angstrom per second
    integral_trans *= const.c.to('cm/s') * 1.0E8
    # Computing a spectral flux density in erg.s^-1.cm^-2.Hz^-1
    # Assumed to be constant over all the spectral range of the filter
    dflux_dnu = 1e-16 * flux / integral_trans
    # Computing a AB magnitude in the filter defined in sed_filter_data
    photomag = -2.5 * math.log10(dflux_dnu) - 48.60
    return photomag


def __flux_from_magnitude(mag_ab, filter_lambda, filter_trans):
    # Computing flux limit in erg.s^-1.cm^-2.Hz^-1.arcsecond^-2
    dflux_dnu = 10.0**(-(48.6 + mag_ab) / 2.5)
    # Integrating the transmission of the filter over its spectral range
    # integral(T(nu),nu) = integral(-T(lambda)*c/lambda^2,lambda)
    integral_trans = np.trapz(
        filter_trans / np.power(filter_lambda, 2), filter_lambda)
    integral_trans *= const.c.to('cm/s') * 1.0E8
    flux_limit = 1.0e16 * dflux_dnu * integral_trans * spatial_sampl**2


def __aurora_version():

    print '   ___				'
    print '  / _ |__ _________  _______ _	'
    print ' / __ / // / __/ _ \/ __/ _ `/	'
    print '/_/ |_\___/_/  \___/_/  \___/ 	'
    print '////// Version 2.0'


def maps(ConfigFile, presets="sinfoni"):
    __aurora_version()
    print '////// Aurora - Maps module'

    geom = geometry_obj()
    geom.parse_input(ConfigFile)

    run = run_obj()
    run.parse_input(ConfigFile)

    maps = maps_obj()
    maps.parse_input(ConfigFile)
    # Defining physical constants
    aconst = constants_obj()
    for ifile in range(len(run.input_file)):

        # Reading input file
        print "// --------------------------------------------------"
        try:
            print "// Pynbody -> Reading file {0} [{1}/{2}]".format(run.input_file[ifile], ifile + 1, len(run.input_file))
            data = pynbody.load(run.input_file[ifile])
            if(run.script_file != ''):
                with open(run.script_file) as f:
                    line = f.read()
                    print "// --------------------------------------------------"
                    print '// Executing script '
                    print "// --------------------------------------------------"
                    print line
                    exec(line)
                    print "// --------------------------------------------------"
        except IOError:
            print '// The input file specified cannot be read'
            sys.exit()

        for i in range(len(maps.gas_keys)):
            try:
                temp = data.gas[maps.gas_keys[i]]
                del temp
                maps.gas_keys_bool[i] = 1
            except:
                maps.gas_keys_bool[i] = 0

        for i in range(len(maps.star_keys)):
            try:
                temp = data.star[maps.star_keys[i]]
                del temp
                maps.star_keys_bool[i] = 1
            except:
                maps.star_keys_bool[i] = 0

        for i in range(len(maps.dm_keys)):
            try:
                temp = data.dm[maps.dm_keys[i]]
                del temp
                maps.dm_keys_bool[i] = 1
            except:
                maps.dm_keys_bool[i] = 0

        if(len(data.g) > 0):
            if((len(np.unique(data.gas['smooth'])) > 1) & (len(np.unique(data.gas['smooth'])) < 20)):
                run.fft_hsml_limits = 1.01 * \
                    np.sort(np.unique(data.gas['smooth']).in_units('kpc'))
                run.nfft = len(run.fft_hsml_limits)
            else:
                run.fft_hsml_limits = np.arange(1.0, run.nfft + 1)
                run.fft_hsml_limits *= run.fft_hsml_min.to('kpc')
        else:
            run.fft_hsml_limits = np.arange(1.0, run.nfft + 1)
            run.fft_hsml_limits *= run.fft_hsml_min.to('kpc')
	print '// smoothing groups',run.fft_hsml_min
        print '// ' + str(run.nfft).strip() + ' levels for adaptive smoothing'

        # Set a coherent unit system
        data.physical_units(velocity='km s**-1',
                            distance='kpc', mass='1.99e+43 g')

        # Re-centering
        # Grid code case - recenter on the simulation box center
        try:
            data['pos']-=data.properties['boxsize']/2.0
        except:
            pass
        if(geom.barycenter):
            print '// Centering'
            temp = data.gas['mass']
            del temp
            pynbody.analysis.halo.center(data)
            print '// Aligning the disk'
            pynbody.analysis.angmom.faceon(data)
        else:
            data["x"] -= geom.centerx
            data["y"] -= geom.centery
            data["z"] -= geom.centerz

        if((geom.phi != 0)):
            data.rotate_y(geom.phi.value)
            print '// Rotating along y ' + str(geom.phi).strip()
        if((geom.theta != 0)):
            data.rotate_z(geom.theta.value)
            print '// Rotating along z ' + str(geom.theta).strip()

        # Applying coordinates filter
        ok = np.where((data["x"] >= geom.amin.value) & (data["x"] <= geom.amax.value) & (data["y"] >= geom.bmin.value) & (
            data["y"] <= geom.bmax.value) & (data["z"] >= geom.cmin.value) & (data["z"] <= geom.cmax.value))
        data = data[ok]
        del ok

        # Computing cosmological luminous distance
        geom.dl = cosmo.luminosity_distance(geom.redshift)
        # spectrom_pixsize is the size of the pixel in kpc
        if(geom.pixsize <= 0.0):
            geom.pixsize = maps.spatial_sampl * \
                (cosmo.kpc_proper_per_arcmin(geom.redshift).to('pc / arcsec'))
        else:
            maps.spatial_sampl = (
                geom.pixsize / (cosmo.kpc_proper_per_arcmin(geom.redshift).to('pc / arcsec')))
        print "// Pixel size -> " + str(geom.pixsize).strip()
        print("// Cosmological Luminous distance -> %10.2f Mpc" %
              geom.dl.to('Mpc').value)

        data_gas = data.gas
        data_star = data.star
        data_dm = data.dm
        del data

        for ilos in range(len(geom.los)):
            print '// LOS = ' + geom.los[ilos]
            # LOS is always Z axis
            if((geom.los[ilos] != 'x') & (geom.los[ilos] != 'y') & (geom.los[ilos] != 'z')):
                print '// ' + geom.los[ilos] + ' is not a valid LOS'
                sys.exit()

            if(geom.los[ilos] == 'x'):
                data_gas["pos"] = np.roll(data_gas["pos"], 2, axis=1)
                data_gas["vel"] = np.roll(data_gas["vel"], 2, axis=1)
                data_star["pos"] = np.roll(data_star["pos"], 2, axis=1)
                data_star["vel"] = np.roll(data_star["vel"], 2, axis=1)
                data_dm["pos"] = np.roll(data_dm["pos"], 2, axis=1)
                data_dm["vel"] = np.roll(data_dm["vel"], 2, axis=1)
            if(geom.los[ilos] == 'y'):
                data_gas["pos"] = np.roll(data_gas["pos"], 1, axis=1)
                data_gas["vel"] = np.roll(data_gas["vel"], 1, axis=1)
                data_star["pos"] = np.roll(data_star["pos"], 1, axis=1)
                data_star["vel"] = np.roll(data_star["vel"], 1, axis=1)
                data_dm["pos"] = np.roll(data_dm["pos"], 1, axis=1)
                data_dm["vel"] = np.roll(data_dm["vel"], 1, axis=1)

            if(run.gas_minmax_keys != ''):
                ngas = len(data_gas)
                for i in range(len(run.gas_minmax_keys)):
                    print '// Restraining gas data to {0:.2e}<{1}<{2:.2e}'.format(run.gas_min_values[i], run.gas_minmax_keys[i], run.gas_max_values[i])
                    ok = np.where((data_gas[run.gas_minmax_keys[i]] > run.gas_min_values[i]) & (
                        data_gas[run.gas_minmax_keys[i]] < run.gas_max_values[i]))
                    if(ok[0].size == 0):
                        print '// No data within specified boundaries'
                        sys.exit()
                    data_gas = data_gas[ok[0]]
                del ok
            ngas = len(data_gas)
            if(maps.gas_keys[0] != ''):
                print '// ', ngas, ' gas elements selected'

            if(run.star_minmax_keys != ''):
                nstar = len(data_star)
                for i in range(len(run.star_minmax_keys)):
                    print '// Restraining star data to {0:.2e}<{1}<{2:.2e}'.format(run.star_min_values[i], run.star_minmax_keys[i], run.star_max_values[i])
                    ok = np.where((data_star[run.star_minmax_keys[i]] > run.star_min_values[i]) & (
                        data_star[run.star_minmax_keys[i]] < run.star_max_values[i]))
                    if(ok[0].size == 0):
                        print '// No data within specified boundaries'
                        sys.exit()
                    data_star = data_star[ok[0]]
                del ok
            nstar = len(data_star)
            if(maps.star_keys[0] != ''):
                print '// ', nstar, ' star elements selected'

            if(run.dm_minmax_keys != ''):
                ndm = len(data_star)
                for i in range(len(run.dm_minmax_keys)):
                    print '// Restraining dm data to {0:.2e}<{1}<{2:.2e}'.format(run.dm_min_values[i], run.dm_minmax_keys[i], run.dm_max_values[i])
                    ok = np.where((data_dm[run.dm_minmax_keys[i]] > run.dm_min_values[i]) & (
                        data_dm[run.dm_minmax_keys[i]] < run.dm_max_values[i]))
                    if(ok[0].size == 0):
                        print '// No data within specified boundaries'
                        sys.exit()
                    data_dm = data_dm[ok[0]]
                del ok
            ndm = len(data_dm)
            if(maps.dm_keys[0] != ''):
                print '// ', ndm, ' dm elements selected'

            print '// ', run.ncpu, ' threads'

            data_gas['pos'] += 0.1*np.random.randn(np.array(data_gas['pos']).shape[0],np.array(data_gas['pos']).shape[1])*np.transpose(np.tile(data_gas['smooth'],(3,1)))

            for i in range(len(maps.gas_keys)):

                maps_dir = run.input_file[ifile].split(
                    '.')[0] + '_maps_nx' + str(maps.nx) + 'ny' + str(maps.ny)

                if(maps.gas_keys_bool[i]):
                    if(geom.redshift < 0.01):
                        output_dir = 'z' + str(int(geom.redshift * 1000) / 1000.) + '_gas_theta' + str(
                            int(geom.theta.value)) + '_phi' + str(int(geom.phi.value)) + '_los' + geom.los[ilos]
                    else:
                        output_dir = 'z' + str(int(geom.redshift * 100) / 100.) + '_gas_theta' + str(
                            int(geom.theta.value)) + '_phi' + str(int(geom.phi.value)) + '_los' + geom.los[ilos]
                    if(maps.gas_method_keys[i] == ''):
                        name = maps.gas_keys[
                            i].strip() + '_' + maps.gas_methods[i].strip() + '.fits'
                    else:
                        name = maps.gas_keys[i].strip(
                        ) + '_' + maps.gas_methods[i].strip() + '_' + maps.gas_method_keys[i].strip() + '.fits'
                    output = maps_dir + '/' + output_dir + '/' + name
                    if((os.path.isfile(output))and(not run.overwrite)):
                        print '// ' + output + ' exists'
                        print '// Skipping'
                        continue

                    if(run.ncpu > 1):
                        pool = mp.Pool(processes=run.ncpu)
                    key_map = np.zeros((maps.nx, maps.ny, run.nfft))
                    mean_map = np.zeros((maps.nx, maps.ny, run.nfft))
                    min_map = np.zeros((maps.nx, maps.ny, run.nfft))
                    max_map = np.zeros((maps.nx, maps.ny, run.nfft))
                    if(maps.gas_methods[i] != "sum"):
                        max_map[:, :, :] = np.min(
                            data_gas[maps.gas_method_keys[i]])
                        min_map[:, :, :] = np.max(
                            data_gas[maps.gas_method_keys[i]])

                    nchunk = int(
                        math.ceil(np.size(data_gas[maps.gas_keys[i]]) / float(run.nvector)))
                    print '// nchunk = ', nchunk
                    print '// Key =', maps.gas_keys[i].strip(), ' method =', maps.gas_methods[i].strip(), ' method_key =', maps.gas_method_keys[i].strip()
                    if(maps.gas_methods[i] == "min"):
                        results = []
                        n = 0
                        for k in range(nchunk):
                            if(run.ncpu > 1):
                                start = k * run.nvector
                                stop = start + run.nvector
                                results.append(pool.apply_async(__project_quantity_min, [geom, run, maps, data_gas["x"][start:stop], data_gas["y"][start:stop], data_gas[
                                               "z"][start:stop], data_gas[maps.gas_method_keys[i]][start:stop], data_gas[maps.gas_keys[i]][start:stop], data_gas["smooth"][start:stop]]))
                                update_progress(float(n) / nchunk)
                                sys.stdout.flush()
                                # Check completion of previous threads
                                to_del = []
                                for l in range(len(results) - 1, -1, -1):
                                    if(results[l].ready()):
                                        temp1, temp2 = results[l].get()
                                        if(n == 0):
                                            key_map = temp1
                                            min_map = temp2
                                        else:
                                            ok_min = np.where(temp1 > key_map)
                                            key_map[ok_min] = temp1[ok_min]
                                            min_map[ok_min] = temp2[ok_min]
                                        to_del.append(l)
                                        n = n + 1
                                # Delete completed threads
                                if(len(to_del) > 0):
                                    for l in to_del:
                                        del results[l]
                            else:
                                start = k * run.nvector
                                stop = start + run.nvector
                                temp1, temp2 = __project_quantity_min(geom, run, maps, data_gas["x"][start:stop], data_gas["y"][start:stop], data_gas["z"][
                                                                      start:stop], data_gas[maps.gas_method_keys[i]][start:stop], data_gas[maps.gas_keys[i]][start:stop], data_gas["smooth"][start:stop])
                                if(n == 0):
                                    key_map = temp1
                                    min_map = temp2
                                else:
                                    ok_min = np.where(temp1 > key_map)
                                    key_map[ok_min] = temp1[ok_min]
                                    min_map[ok_min] = temp2[ok_min]
                                key_map = temp1
                                min_map = temp2

                        if(run.ncpu > 1):
                            while True:
                                update_progress(float(n) / nchunk)
                                sys.stdout.flush()
                                to_del = []
                                for l in range(len(results) - 1, -1, -1):
                                    if(results[l].ready()):
                                        temp1, temp2 = results[l].get()
                                        if(n == 0):
                                            key_map = temp1
                                            min_map = temp2
                                        else:
                                            ok_min = np.where(temp1 > key_map)
                                            key_map[ok_min] = temp1[ok_min]
                                            min_map[ok_min] = temp2[ok_min]
                                        to_del.append(l)
                                        n = n + 1
                                # Delete completed threads
                                if(len(to_del) > 0):
                                    for l in to_del:
                                        del results[l]
                                if(n == nchunk):
                                    update_progress(float(n) / nchunk)
                                    sys.stdout.flush()
                                    break
                            pool.close()
                            pool.join()
                        else:
                            update_progress(1)

                        print '// map convolution'
                        __map_adaptive_convolution(key_map, run.nfft, run.fft_hsml_limits, geom.pixsize.to(
                            'kpc').value, maps.gas_methods[i], run.kernel_scale)
                        __map_adaptive_convolution(min_map, run.nfft, run.fft_hsml_limits, geom.pixsize.to(
                            'kpc').value, maps.gas_methods[i], run.kernel_scale)
                        for j in range(maps.nx):
                            for k in range(maps.ny):
                                to_keep = np.argmin(min_map[j, k, :])
                                key_map[j, k, 0] = key_map[j, k, to_keep]
                        key_map = key_map[:, :, 0]
                        __write_fits_map(geom, maps, run, key_map, maps.gas_keys[i], maps.gas_methods[
                                         i], maps.gas_method_keys[i], 'gas', ifile, ilos)

                    if(maps.gas_methods[i] == "max"):
                        results = []
                        n = 0
                        for k in range(nchunk):
                            if(run.ncpu > 1):
                                start = k * run.nvector
                                stop = start + run.nvector
                                results.append(pool.apply_async(__project_quantity_max, [geom, run, maps, data_gas["x"][start:stop], data_gas["y"][start:stop], data_gas[
                                               "z"][start:stop], data_gas[maps.gas_method_keys[i]][start:stop], data_gas[maps.gas_keys[i]][start:stop], data_gas["smooth"][start:stop]]))
                                update_progress(float(n) / nchunk)
                                sys.stdout.flush()
                                # Check completion of previous threads
                                to_del = []
                                for l in range(len(results) - 1, -1, -1):
                                    if(results[l].ready()):
                                        temp1, temp2 = results[l].get()
                                        if(n == 0):
                                            key_map = temp1
                                            max_map = temp2
                                        else:
                                            ok_max = np.where(temp1 > key_map)
                                            key_map[ok_max] = temp1[ok_max]
                                            max_map[ok_max] = temp2[ok_max]
                                        to_del.append(l)
                                        n = n + 1
                                # Delete completed threads
                                if(len(to_del) > 0):
                                    for l in to_del:
                                        del results[l]
                            else:
                                start = k * run.nvector
                                stop = start + run.nvector
                                temp1, temp2 = __project_quantity_max(geom, run, maps, data_gas["x"][start:stop], data_gas["y"][start:stop], data_gas["z"][
                                                                      start:stop], data_gas[maps.gas_method_keys[i]][start:stop], data_gas[maps.gas_keys[i]][start:stop], data_gas["smooth"][start:stop])
                                if(k == 0):
                                    key_map = temp1
                                    max_map = temp2
                                else:
                                    ok_max = np.where(temp1 > key_map)
                                    key_map[ok_max] = temp1[ok_max]
                                    max_map[ok_max] = temp2[ok_max]

                        if(run.ncpu > 1):
                            while True:
                                update_progress(float(n) / nchunk)
                                sys.stdout.flush()
                                to_del = []
                                for l in range(len(results) - 1, -1, -1):
                                    if(results[l].ready()):
                                        temp1, temp2 = results[l].get()
                                        if(n == 0):
                                            key_map = temp1
                                            max_map = temp2
                                        else:
                                            ok_max = np.where(temp1 > key_map)
                                            key_map[ok_max] = temp1[ok_max]
                                            max_map[ok_max] = temp2[ok_max]
                                        to_del.append(l)
                                        n = n + 1
                                # Delete completed threads
                                if(len(to_del) > 0):
                                    for l in to_del:
                                        del results[l]
                                if(n == nchunk):
                                    update_progress(float(n) / nchunk)
                                    sys.stdout.flush()
                                    break
                            pool.close()
                            pool.join()
                        else:
                            update_progress(1)

                        print '// map convolution'
                        non_null = np.where(key_map != 0.)
                        key_map[non_null] = 1.0 / key_map[non_null]
                        __map_adaptive_convolution(key_map, run.nfft, run.fft_hsml_limits, geom.pixsize.to(
                            'kpc').value, maps.gas_methods[i], run.kernel_scale)
                        __map_adaptive_convolution(max_map, run.nfft, run.fft_hsml_limits, geom.pixsize.to(
                            'kpc').value, maps.gas_methods[i], run.kernel_scale)
                        key_map[non_null] = 1.0 / key_map[non_null]
                        for j in range(maps.nx):
                            for k in range(maps.ny):
                                to_keep = np.argmax(max_map[k, j, :])
                                key_map[k, j, 0] = key_map[k, j, to_keep]
                        key_map = key_map[:, :, 0]
                        __write_fits_map(geom, maps, run, key_map, maps.gas_keys[i], maps.gas_methods[
                                         i], maps.gas_method_keys[i], 'gas', ifile, ilos)

                    if(maps.gas_methods[i] == "mean"):
                        results = []
                        n = 0
                        ndel = 0
                        for k in range(nchunk):
                            if(run.ncpu > 1):
                                start = k * run.nvector
                                stop = start + run.nvector
                                x = np.array(data_gas["x"][start:stop])
                                y = np.array(data_gas["y"][start:stop])
                                z = np.array(data_gas["z"][start:stop])
                                mean = np.array(
                                    data_gas[maps.gas_method_keys[i]][start:stop])
                                quantity = np.array(
                                    data_gas[maps.gas_keys[i]][start:stop])
                                temp = np.mean(mean)
                                temp = np.mean(quantity)
                                smooth = np.array(
                                    data_gas["smooth"][start:stop])
                                results.append(pool.apply_async(__project_quantity_mean, [
                                               geom, run, maps, x, y, z, mean, quantity, smooth]))
                                update_progress(float(n) / nchunk)
                                sys.stdout.flush()
                                # Check completion of previous threads
                                to_del = []
                                for l in range(len(results) - 1, -1, -1):
                                    if(results[l].ready()):
                                        temp1, temp2 = results[l].get()
                                        key_map += temp1
                                        mean_map += temp2
                                        to_del.append(l)
                                        n = n + 1
                                # Delete completed threads
                                if(len(to_del) > 0):
                                    for l in to_del:
                                        del results[l]
                            else:
                                start = k * run.nvector
                                stop = start + run.nvector
                                temp1, temp2 = __project_quantity_mean(geom, run, maps, data_gas["x"][start:stop], data_gas["y"][start:stop], data_gas["z"][
                                                                       start:stop], data_gas[maps.gas_method_keys[i]][start:stop], data_gas[maps.gas_keys[i]][start:stop], data_gas["smooth"][start:stop])
                                update_progress(float(k) / nchunk)
                                sys.stdout.flush()
                                key_map += temp1
                                mean_map += temp2

                        if(run.ncpu > 1):
                            while True:
                                n = sum(1 for x in results if not x.ready())
                                update_progress(float(nchunk - n) / nchunk)
                                sys.stdout.flush()
                                if(n == 0):
                                    break

                            for x in results:
                                temp1, temp2 = x.get()
                                key_map += temp1
                                mean_map += temp2
                            del results
                            pool.close()
                            pool.join()
                        else:
                            update_progress(1)

                        print '// map convolution'
                        __map_adaptive_convolution(key_map, run.nfft, run.fft_hsml_limits, geom.pixsize.to(
                            'kpc').value, maps.gas_methods[i], run.kernel_scale)
                        __map_adaptive_convolution(mean_map, run.nfft, run.fft_hsml_limits, geom.pixsize.to(
                            'kpc').value, maps.gas_methods[i], run.kernel_scale)
                        mean_map = np.sum(mean_map, axis=2)
                        key_map = np.sum(key_map, axis=2)
                        defined = np.where(mean_map > 1e-10)
                        key_map[defined] /= mean_map[defined]
                        __write_fits_map(geom, maps, run, key_map, maps.gas_keys[i], maps.gas_methods[
                                         i], maps.gas_method_keys[i], 'gas', ifile, ilos)

                    if(maps.gas_methods[i] == "sum"):
                        maps.gas_method_keys[i] = ''
                        key_map = __project_quantity_sum(geom, run, maps, data_gas["x"], data_gas[
                                                         "y"], data_gas["z"], data_gas[maps.gas_keys[i]], data_gas["smooth"])
                        __map_adaptive_convolution(key_map, run.nfft, run.fft_hsml_limits, geom.pixsize.to(
                            'kpc').value, maps.gas_methods[i], run.kernel_scale)
                        key_map = np.sum(key_map, axis=2)
                        __write_fits_map(geom, maps, run, key_map, maps.gas_keys[i], maps.gas_methods[
                                         i], maps.gas_method_keys[i], 'gas', ifile, ilos)
		else:
			print '// Key "',maps.gas_keys[i],'" does not exist'

            for i in range(len(maps.star_keys)):
                if(maps.star_keys_bool[i]):
                    print '// Key =', maps.star_keys[i], ' method =', maps.star_methods[i], ' method_key =', maps.star_method_keys[i].strip()
                    if(maps.star_methods[i] == "min"):
                        key_map, min_map = __project_quantity_min(geom, run, maps, data_star["x"], data_star["y"], data_star[
                                                                  "z"], data_star[maps.star_method_keys[i]], data_star[maps.star_keys[i]], data_star["smooth"])
                        __map_adaptive_convolution(key_map, run.nfft, run.fft_hsml_limits, geom.pixsize.to(
                            'kpc').value, maps.star_methods[i], run.kernel_scale)
                        __map_adaptive_convolution(min_map, run.nfft, run.fft_hsml_limits, geom.pixsize.to(
                            'kpc').value, maps.star_methods[i], run.kernel_scale)
                        for j in range(maps.nx):
                            for k in range(maps.ny):
                                to_keep = np.argmin(min_map[k, j, :])
                                key_map[k, j, 0] = key_map[k, j, to_keep]
                        key_map = key_map[:, :, 0]
                        __write_fits_map(geom, maps, run, key_map, maps.star_keys[i], maps.star_methods[
                                         i], maps.star_method_keys[i], 'stars', ifile, ilos)

                    if(maps.star_methods[i] == "max"):
                        key_map, max_map = __project_quantity_max(geom, run, maps, data_star["x"], data_star["y"], data_star[
                                                                  "z"], data_star[maps.star_method_keys[i]], data_star[maps.star_keys[i]], data_star["smooth"])
                        __map_adaptive_convolution(key_map, run.nfft, fft_hsml_limits, geom.pixsize.to(
                            'kpc').value, maps.star_methods[i], run.kernel_scale)
                        __map_adaptive_convolution(max_map, run.nfft, fft_hsml_limits, geom.pixsize.to(
                            'kpc').value, maps.star_methods[i], run.kernel_scale)
                        for j in range(maps.nx):
                            for k in range(maps.ny):
                                to_keep = np.argmax(max_map[k, j, :])
                                key_map[j, i, 0] = key_map[j, i, to_keep]
                        key_map = key_map[:, :, 0]
                        __write_fits_map(geom, maps, run, key_map, maps.star_keys[i], maps.star_methods[
                                         i], maps.star_method_keys[i], 'stars', ifile, ilos)

                    if(maps.star_methods[i] == "mean"):
                        key_map, mean_map = __project_quantity_mean(geom, run, maps, data_star["x"], data_star["y"], data_star[
                                                                    "z"], data_star[maps.star_method_keys[i]], data_star[maps.star_keys[i]], data_star["smooth"])
                        __map_adaptive_convolution(key_map, run.nfft, run.fft_hsml_limits, geom.pixsize.to(
                            'kpc').value, maps.star_methods[i], run.kernel_scale)
                        __map_adaptive_convolution(mean_map, run.nfft, run.fft_hsml_limits, geom.pixsize.to(
                            'kpc').value, maps.star_methods[i], run.kernel_scale)
                        key_map = np.sum(key_map, axis=2)
                        mean_map = np.sum(mean_map, axis=2)
                        defined = np.where(mean_map > 1e-10)
                        key_map[defined] /= mean_map[defined]
                        __write_fits_map(geom, maps, run, key_map, maps.star_keys[i], maps.star_methods[
                                         i], maps.star_method_keys[i], 'stars', ifile, ilos)

                    if(maps.star_methods[i] == "sum"):
                        maps.star_method_keys[i] = ''
                        key_map = __project_quantity_sum(geom, run, maps, data_star["x"], data_star[
                                                         "y"], data_star["z"], data_star[maps.star_keys[i]], data_star["smooth"])
                        __map_adaptive_convolution(key_map, run.nfft, run.fft_hsml_limits, geom.pixsize.to(
                            'kpc').value, maps.star_methods[i], run.kernel_scale)
                        key_map = np.sum(key_map, axis=2)
                        __write_fits_map(geom, maps, run, key_map, maps.star_keys[i], maps.star_methods[
                                         i], maps.star_method_keys[i], 'stars', ifile, ilos)
		else:
			print '// Key "',maps.star_keys[i],'" does not exist'

            for i in range(len(maps.dm_keys)):
                if(maps.dm_keys_bool[i]):
                    print '// Key =', maps.dm_keys[i], ' method =', maps.dm_methods[i], ' method_key =', maps.dm_method_keys[i].strip()
                    if(maps.dm_methods[i] == "min"):
                        key_map, min_map = __project_quantity_min(geom, run, maps, data_dm["x"], data_dm["y"], data_dm[
                                                                  "z"], data_dm[maps.dm_method_keys[i]], data_dm[maps.dm_keys[i]], data_dm["smooth"])
                        __map_adaptive_convolution(key_map, nfft, fft_hsml_limits.to(
                            'kpc').value, pixsize.to('kpc').value, maps.dm_methods[i], run.kernel_scale)
                        __map_adaptive_convolution(min_map, nfft, fft_hsml_limits.to(
                            'kpc').value, pixsize.to('kpc').value, maps.dm_methods[i], run.kernel_scale)
                        for j in range(maps.nx):
                            for k in range(maps.ny):
                                to_keep = np.argmin(min_map[k, j, :])
                                key_map[k, j, 0] = key_map[k, j, to_keep]
                        key_map = key_map[:, :, 0]
                        __write_fits_map(geom, maps, run, key_map, maps.dm_keys[i], maps.dm_methods[
                                         i], maps.dm_method_keys[i], 'dm', ifile, ilos)

                    if(maps.dm_methods[i] == "max"):
                        key_map, max_map = __project_quantity_max(geom, run, maps, data_dm["x"], data_dm["y"], data_dm[
                                                                  "z"], data_dm[maps.dm_method_keys[i]], data_dm[maps.dm_keys[i]], data_dm["smooth"])
                        __map_adaptive_convolution(key_map, nfft, fft_hsml_limits.to(
                            'kpc').value, pixsize.to('kpc').value, maps.dm_methods[i], run.kernel_scale)
                        __map_adaptive_convolution(max_map, nfft, fft_hsml_limits.to(
                            'kpc').value, pixsize.to('kpc').value, maps.dm_methods[i], run.kernel_scale)
                        for j in range(maps.nx):
                            for k in range(maps.ny):
                                to_keep = np.argmax(max_map[k, j, :])
                                key_map[k, j, 0] = key_map[k, j, to_keep]
                        key_map = key_map[:, :, 0]
                        __write_fits_map(geom, maps, run, key_map, maps.dm_keys[i], maps.dm_method[
                                         i], maps.dm_method_keys[i], 'dm', ifile, ilos)

                    if(maps.dm_methods[i] == "mean"):
                        key_map, mean_map = __project_quantity_mean(geom, run, maps, data_dm["x"], data_dm["y"], data_dm["z"], data_dm[
                                                                    maps.dm_method_keys[i]], data_dm[maps.dm_keys[i]], data_dm["smooth"], spatial_dim, spatial_dim, pixsize, nvector)
                        __map_adaptive_convolution(key_map, run.nfft, run.fft_hsml_limits.to(
                            'kpc').value, geom.pixsize.to('kpc').value, maps.dm_methods[i], run.kernel_scale)
                        __map_adaptive_convolution(mean_map, run.nfft, run.fft_hsml_limits.to(
                            'kpc').value, geom.pixsize.to('kpc').value, maps.dm_methods[i], run.kernel_scale)
                        key_map = np.sum(key_map, axis=2)
                        mean_map = np.sum(mean_map, axis=2)
                        defined = np.where(mean_map > 1e-10)
                        key_map[defined] /= mean_map[defined]
                        __write_fits_map(geom, maps, run, key_map, maps.dm_keys[i], maps.dm_methods[
                                         i], maps.dm_method_keys[i], 'dm', ifile, ilos)
		else:
			print '// Key "',maps.dm_keys[i],'" does not exist'

            if(geom.los[ilos] == 'x'):
                data_gas["pos"] = np.roll(data_gas["pos"], -2, axis=1)
                data_gas["vel"] = np.roll(data_gas["vel"], -2, axis=1)
                data_star["pos"] = np.roll(data_star["pos"], -2, axis=1)
                data_star["vel"] = np.roll(data_star["vel"], -2, axis=1)
                data_dm["pos"] = np.roll(data_dm["pos"], -2, axis=1)
                data_dm["vel"] = np.roll(data_dm["vel"], -2, axis=1)
            if(geom.los[ilos] == 'y'):
                data_gas["pos"] = np.roll(data_gas["pos"], -1, axis=1)
                data_gas["vel"] = np.roll(data_gas["vel"], -1, axis=1)
                data_star["pos"] = np.roll(data_star["pos"], -1, axis=1)
                data_star["vel"] = np.roll(data_star["vel"], -1, axis=1)
                data_dm["pos"] = np.roll(data_dm["pos"], -1, axis=1)
                data_dm["vel"] = np.roll(data_dm["vel"], -1, axis=1)

            geom.output_dir = ''


def photom_mock(ConfigFile):
    __aurora_version()
    print '////// Aurora - Photometry mock module'

    geom = geometry_obj()
    if(not os.path.isfile(ConfigFile)):
        print '// ' + ConfigFile + ' not found'
        return
    geom.parse_input(ConfigFile)

    run = run_obj()
    run.parse_input(ConfigFile)

    photom = photom_obj()
    photom.parse_input(ConfigFile)
    # Defining physical constants
    aconst = constants_obj()
    for ifile in range(len(run.input_file)):
        # Reading simulation input file
        try:
            print "// Pynbody -> Reading file " + run.input_file[ifile]
            data = pynbody.load(run.input_file[ifile])
        except IOError:
            print '// The input file specified cannot be read'
            return

        # Set a coherent unit system
        data.physical_units(velocity='km s**-1',
                            distance='kpc', mass='1.99e+43 g')
        #data.physical_units(velocity='cm s**-1', distance='cm', mass='g')

        # Re-centering
        # Grid code case - recenter on the simulation box center
        try:
            data['pos']-=data.properties['boxsize']/2.0
        except:
            pass
        if(geom.barycenter):
            print '// Centering'
            temp = data.gas['mass']
            del temp
            pynbody.analysis.halo.center(data)
            print '// Aligning the disk'
            pynbody.analysis.angmom.faceon(data)
        else:
            data["x"] -= geom.centerx
            data["y"] -= geom.centery
            data["z"] -= geom.centerz

        if((geom.phi != 0)):
            data.rotate_y(geom.phi.value)
            print '// Rotating along y ' + str(geom.phi).strip()
        if((geom.theta != 0)):
            data.rotate_z(geom.theta.value)
            print '// Rotating along z ' + str(geom.theta).strip()

        # Applying coordinates filter
        ok = np.where((data['x'] >= geom.amin.value) & (data['x'] <= geom.amax.value) &
                      (data['y'] >= geom.bmin.value) & (data['y'] <= geom.bmax.value) &
                      (data['z'] >= geom.cmin.value) & (data['z'] <= geom.cmax.value))
        data = data[ok]
        del ok

        # Computing cosmological luminous distance
        geom.dl = cosmo.luminosity_distance(geom.redshift)
        # spectrom_pixsize is the size of the pixel in kpc
        if(geom.pixsize <= 0.0):
            geom.pixsize = maps.spatial_sampl * \
                (cosmo.kpc_proper_per_arcmin(geom.redshift).to('pc / arcsec'))
        else:
            photom.spatial_sampl = (
                geom.pixsize / (cosmo.kpc_proper_per_arcmin(geom.redshift).to('pc / arcsec')))
        print "// Pixel size -> " + str(geom.pixsize).strip()
        print("// Cosmological Luminous distance -> %10.2f Mpc" %
              geom.dl.to('Mpc').value)
	print "// Spatial resolution ->",photom.spatial_res
	print "// Spatial sampl      ->",photom.spatial_sampl

        data_gas = data.gas
        data_star = data.star
        data_dm = data.dm
        # Check for star particles hidden in dm family
        #ok_st 		= np.where((data.dm['tform']!=0)&(data.dm['mass']>0))
        #ok_dm 		= np.where((data.dm['tform']==0)&(data.dm['mass']>0))
        # if(len(ok_st[0])>0):
        #	data_star 	= data_dm[ok_st]
        # if(len(ok_st[0])>0):
        #	data_dm 	= data_dm[ok_dm]
        del data
        del data_dm
        gc.collect()

        if(run.star_minmax_keys != ''):
            for i in range(len(run.star_minmax_keys)):
                print '// Restraining data to {0:.2e}<{1}<{2:.2e}'.format(run.star_min_values[i], run.star_minmax_keys[i], run.star_max_values[i])
                print '// Min=', np.min(data_stars[run.star_minmax_keys[i]]), 'Max=', np.max(data_stars[run.star_minmax_keys[i]])
                ok = np.where((data_star[run.stars_minmax_keys[i]] > run.star_min_values[i]) & (
                    data_stars[run.star_minmax_keys[i]] < run.star_max_values[i]))
                if(ok[0].size == 0):
                    print '// No data within specified boundaries'
                    sys.exit()
                data_star = data_star[ok[0]]
            print '// ', len(ok[0]), ' elements selected'
            del ok
        if(photom.dust_to_gas > 0):
            if(run.gas_minmax_keys != ''):
                for i in range(len(run.gas_minmax_keys)):
                    print '// Restraining gas data to {0:.2e}<{1}<{2:.2e}'.format(run.gas_min_values[i], run.gas_minmax_keys[i], run.gas_max_values[i])
                    ok = np.where((data_gas[run.gas_minmax_keys[i]] > run.gas_min_values[i]) & (
                        data_gas[run.gas_minmax_keys[i]] < run.gas_max_values[i]))
                    if(ok[0].size == 0):
                        print '// No data within specified boundaries'
                        sys.exit()
                    data_gas = data_gas[ok[0]]
                print '// ', len(ok[0]), ' elements selected'
                del ok
            run.fft_hsml_limits = 1.25 * \
                np.sort(np.unique(data_gas['smooth']).in_units('kpc'))
            run.nfft = len(run.fft_hsml_limits)
        else:
            del data_gas
            gc.collect()

        # Sorting particles by age
        age_sorted = np.argsort(data_star["age"])
        data_star = data_star[age_sorted]
        nstars = len(data_star)
        print '// nstars = ', nstars

        # Reading SED model file
        sed_model_tab = np.loadtxt(photom.sed_file)

        for ilos in range(len(geom.los)):
            print '// LOS = ' + geom.los[ilos]
            # LOS is always Z axis
            if((geom.los[ilos] != 'x') & (geom.los[ilos] != 'y') & (geom.los[ilos] != 'z')):
                print '// ' + geom.los[ilos] + ' is not a valid LOS'
                sys.exit()

            if(geom.los[ilos] == 'x'):
                data_star["pos"] = np.roll(data_star["pos"], 2, axis=1)
                data_star["vel"] = np.roll(data_star["vel"], 2, axis=1)
                if(photom.dust_to_gas > 0):
                    data_gas["pos"] = np.roll(data_gas["pos"], 2, axis=1)
                    data_gas["vel"] = np.roll(data_gas["vel"], 2, axis=1)
            if(geom.los[ilos] == 'y'):
                data_star["pos"] = np.roll(data_star["pos"], 1, axis=1)
                data_star["vel"] = np.roll(data_star["vel"], 1, axis=1)
                if(photom.dust_to_gas > 0):
                    data_gas["pos"] = np.roll(data_gas["pos"], 1, axis=1)
                    data_gas["vel"] = np.roll(data_gas["vel"], 1, axis=1)

            pargs = photom_args()

            if(photom.dust_to_gas > 0):
                ok_gas = np.where((data_gas['x'] > np.min(data_star['x'] - np.max(data_gas['smooth'])))
                                  & (data_gas['x'] < np.max(data_star['x'] + np.max(data_gas['smooth'])))
                                  & (data_gas['y'] > np.min(data_star['y'] - np.max(data_gas['smooth'])))
                                  & (data_gas['y'] < np.max(data_star['y'] + np.max(data_gas['smooth'])))
                                  & (data_gas['z'] > np.min(data_star['z'] - np.max(data_gas['smooth']))))
                data_gas = data_gas[ok_gas]

                tree_gas = []
                pargs.data_gas_x = []
                pargs.data_gas_y = []
                pargs.data_gas_z = []
                pargs.data_gas_smooth = []
                pargs.data_gas_metal = []
                pargs.data_gas_rho = []
                pargs.data_gas_mass = []

                for i in range(run.nfft):
                    if(i == 0):
                        ok_level = np.where(
                            (data_gas['smooth'] < run.fft_hsml_limits[i]))
                    elif(i == run.nfft - 1):
                        ok_level = np.where(
                            (data_gas['smooth'] > run.fft_hsml_limits[i - 1]))
                    else:
                        ok_level = np.where((data_gas['smooth'] < run.fft_hsml_limits[i]) & (
                            data_gas['smooth'] > run.fft_hsml_limits[i - 1]))
                    p = zip(np.array(data_gas['x'])[
                            ok_level], np.array(data_gas['y'])[ok_level])
                    if(len(p) > 0):
                        tree_gas.append(KDTree(p))
                        print '// Building Tree Level', i
                        # Store gas particles
                        pargs.data_gas_x.append(
                            np.array(data_gas['x'])[ok_level])
                        pargs.data_gas_y.append(
                            np.array(data_gas['y'])[ok_level])
                        pargs.data_gas_z.append(
                            np.array(data_gas['z'])[ok_level])
                        pargs.data_gas_smooth.append(
                            np.array(data_gas['smooth'])[ok_level])
                        pargs.data_gas_metal.append(
                            np.array(data_gas['metal'])[ok_level])
                        pargs.data_gas_rho.append(
                            np.array(data_gas['rho'].in_units('2.200789e-24 g cm^-3'))[ok_level])
                        pargs.data_gas_mass.append(
                            np.array(data_gas['mass'])[ok_level])

                # Store gas particles tree
                pargs.tree_gas = tree_gas

            for file_index in range(len(photom.filter_files)):
                # Only one AB mag limit is used
                if(len(photom.mag_ab_limits) == 1):
                    mag_ab_limit = float(photom.mag_ab_limits[0])
                # Each filter have its AB mag limit
                else:
                    mag_ab_limit = float(photom.mag_ab_limits[file_index])
                # Set output name
                photom_dir = run.input_file[ifile].split(
                    '.')[0] + '_photom_nx' + str(photom.nx) + 'ny' + str(photom.ny)
                if(geom.output_dir == ''):
                    if(geom.redshift < 0.01):
                        geom.output_dir = 'z' + str(int(geom.redshift * 1000) / 1000.) + '_theta' + str(
                            int(geom.theta.value)) + '_phi' + str(int(geom.phi.value)) + '_los' + geom.los[ilos]
                    else:
                        geom.output_dir = 'z' + str(int(geom.redshift * 100) / 100.) + '_theta' + str(
                            int(geom.theta.value)) + '_phi' + str(int(geom.phi.value)) + '_los' + geom.los[ilos]

                splitted_filter_loc = re.split(
                    '/|\.', photom.filter_files[file_index])
                photom_prefix = splitted_filter_loc[
                    len(splitted_filter_loc) - 2].lower().split('.')[0]
                ext = '_flux'
                if(photom.dust_to_gas > 0):
                    ext = ext + '_dust'
                if(mag_ab_limit > -999):
                    ext = ext + '_noise'
                ext = ext + '_map.fits'
                output_name = photom_dir + '/' + geom.output_dir + '/' + photom_prefix + ext
                # Check if output already exists
                if((not os.path.isfile(output_name))or(run.overwrite)):
                    print '// SED-model file loaded [' + photom.sed_file + ']'
                    # Reading filter transmission file
                    filter_tab = np.loadtxt(photom.filter_files[file_index])
                    photom.filter_lambda = filter_tab[:, 0]
                    photom.filter_trans = filter_tab[:, 1]
                    spectral_domain = np.where((sed_model_tab[:, 1] * (1 + geom.redshift) >= 0.975 * np.min(
                        photom.filter_lambda)) & (sed_model_tab[:, 1] * (1 + geom.redshift) <= 1.025 * np.max(photom.filter_lambda)))
                    sed_model_tab_filter = sed_model_tab[spectral_domain]
                    # SED time expressed in Myr
                    photom.sed_model_time = sed_model_tab_filter[:, 0] / 1.0E6
                    first_timestep = np.where(
                        photom.sed_model_time == photom.sed_model_time[0])
                    # SED wavelength expressed in cm
                    photom.sed_model_lambda = sed_model_tab_filter[:, 1]
                    # Interpolating the filter transmission to the SED model
                    # wavelengths
                    photom.filter_trans = np.interp(photom.sed_model_lambda[
                                                    first_timestep], photom.filter_lambda, photom.filter_trans)
                    photom.filter_lambda = photom.sed_model_lambda[
                        first_timestep]

                    # SED luminosity expressed in erg.s^-1.microns^-1 (expressed in erg.s^-1.angstrom^-1 in the SED file)
                    # We also apply the flux conservation due to redshifting
                    photom.sed_model_flux = np.power(10.0, sed_model_tab_filter[
                                                     :, 2]) + np.power(10.0, sed_model_tab_filter[:, 3])
                    del sed_model_tab_filter
                    # Computing stellar extinction in rest-frame
                    sed_model_lambda_micron = photom.sed_model_lambda * 1.0e-4

                    sed_vis = np.where((sed_model_lambda_micron >= 0.12) & (
                        sed_model_lambda_micron <= 0.63))
                    sed_ir = np.where((sed_model_lambda_micron >= 0.63) & (
                        sed_model_lambda_micron <= 2.20))
                    photom.sed_obscuration = np.zeros(
                        len(photom.sed_model_lambda))

                    if(sed_vis[0].size > 0):
                        photom.sed_obscuration[
                            sed_vis] = 2.659 * (-1.857 + 1.040 / sed_model_lambda_micron[sed_vis]) + photom.rv
                    if(sed_ir[0].size > 0):
                        photom.sed_obscuration[sed_ir] = 2.659 * (-2.156 + 1.509 / sed_model_lambda_micron[sed_ir] - 0.198 / np.power(
                            sed_model_lambda_micron[sed_ir], 2.0) + 0.011 / np.power(sed_model_lambda_micron[sed_ir], 3.0)) + photom.rv

                    del sed_model_lambda_micron, sed_vis, sed_ir

                    # Redshifiting SED (flux conservation)
                    #photom.sed_model_lambda 	*= (1.0+geom.redshift)
                    #photom.sed_model_flux 		/= (1.0+geom.redshift)

                    print '// Filter transmission file loaded [' + photom.filter_files[file_index] + ']'
                    print '// Dust to gas ratio -> ', photom.dust_to_gas, ' mag/cm^2'

                    nchunk = int(math.ceil(nstars / float(run.nvector)))
                    flux_map = np.zeros((photom.nx, photom.ny))
                    if(run.ncpu > 1):
                        pool = mp.Pool(processes=run.ncpu)
                    print '// ', run.ncpu, ' threads'
                    update_progress(0.0)
                    sys.stdout.flush()
                    n = 0

                    if(run.ncpu > 1):
                        results = []
                        for i in range(nchunk):
                            start = i * run.nvector
                            stop = start + min(run.nvector, nstars - start)
                            print start,stop,nstars - start
                            pargs.data_star_x = data_star['x'][start:stop]
                            pargs.data_star_y = data_star['y'][start:stop]
                            pargs.data_star_z = data_star['z'][start:stop]
                            pargs.data_star_vz = data_star['vz'][start:stop]
                            # For some weird reason, the mass and age arrays
                            # need to be accessed to return the right values
                            temp = np.mean(data_star['age'][
                                           start:stop].in_units('Myr'))
                            temp = np.mean(data_star['mass'][
                                           start:stop].in_units('Msol'))
                            pargs.data_star_mass = data_star[
                                'mass'][start:stop].in_units('Msol')
                            pargs.data_star_metal = data_star[
                                'metal'][start:stop]
                            pargs.data_star_age = data_star[
                                'age'][start:stop].in_units('Myr')
                            results.append(pool.apply_async(
                                __project_photom_flux, [geom, photom, run, pargs]))
                            update_progress(float(n) / nchunk)
                            sys.stdout.flush()
                            # Check completion of previous threads
                            to_del = []
                            for k in range(len(results) - 1, -1, -1):
                                if(results[k].ready()):
                                    flux_map += results[k].get()
                                    to_del.append(k)
                                    n = n + 1
                            # Delete completed threads
                            if(len(to_del) > 0):
                                for k in to_del:
                                    del results[k]
                                    gc.collect()

                    else:
                        for i in range(nchunk):
                            start = i * run.nvector
                            stop = start + min(run.nvector, nstars - start)
                            pargs.data_star_x = data_star['x'][start:stop]
                            pargs.data_star_y = data_star['y'][start:stop]
                            pargs.data_star_z = data_star['z'][start:stop]
                            pargs.data_star_vz = data_star['vz'][start:stop]
                            pargs.data_star_mass = data_star[
                                'mass'][start:stop].in_units('Msol')
                            pargs.data_star_metal = data_star[
                                'metal'][start:stop]
                            pargs.data_star_age = data_star[
                                'age'][start:stop].in_units('Myr')
                            flux_map += __project_photom_flux(
                                geom, photom, run, pargs)
                            update_progress(float(i + 1) / nchunk)
                            sys.stdout.flush()
                    if(run.ncpu > 1):
                        while True:
                            n = sum(1 for x in results if not x.ready())
                            update_progress(float(nchunk - n) / nchunk)
                            sys.stdout.flush()
                            if(n == 0):
                                break

                        for x in results:
                            flux_map += x.get()

                        pool.close()
                        pool.join()
                        del results
                        gc.collect()

                    print '// Total flux -> ' + str(np.sum(flux_map)) + ' E-16 erg.s^-1.cm^-2'
                    # Spatial smoothing
                    if(photom.spatial_res > 0):
                        print '// Spatial smoothing'
                        __map_convolution(
                            flux_map, photom.spatial_res.value / photom.spatial_sampl.value, photom.nx, photom.ny)
                    # Noise injection
                    if(mag_ab_limit > -999):
                        # Computing flux limit in
                        # erg.s^-1.cm^-2.Hz^-1.arcsecond^-2
                        print '// Computing Gaussian noise...'
                        flux_limit = __flux_from_magnitude(
                            mag_ab, filter_tab[:, 0], filter_tab[:, 1])
                        # Computing 3 sigma flux limit in 1E-16 erg.s^-1.cm^-2
                        # on one pixel
                        flux_limit /= 3.0
                        print '// Photometric flux limit ' + str(flux_limit) + ' E-16 erg.s^-1.cm^-2'
                        # Noise injection
                        flux_map_noise = flux_map + \
                            np.random.normal(
                                0.0, flux_limit, (photom.nx, photom.ny))
                    else:
                        flux_map_noise = flux_map

                    try:
                        os.mkdir(photom_dir)
                    except:
                        print '// ' + photom_dir + ' exist'

                    try:
                        os.mkdir(photom_dir + '/' + geom.output_dir)
                    except:
                        print '// ' + photom_dir + '/' + geom.output_dir + ' exists'

                    # Writing maps to FITS files
                    hdu = fits.PrimaryHDU(flux_map)
                    hdulist = fits.HDUList([hdu])
                    prihdr = hdu.header

                    prihdr['BITPIX'] = -32
                    prihdr['NAXIS'] = 2
                    prihdr['NAXIS1'] = photom.nx
                    prihdr['NAXIS2'] = photom.ny
                    prihdr['BSCALE'] = 1.0
                    prihdr['BZERO'] = 0
                    prihdr['BUNIT'] = 'E-16 ERG.S^-1.CM^-2'
                    prihdr['CDELT1'] = -photom.spatial_sampl.value / 3600.
                    prihdr['CDELT2'] = photom.spatial_sampl.value / 3600.
                    prihdr['CRPIX1'] = (photom.nx - 1) / 2.
                    prihdr['CRPIX2'] = (photom.ny - 1) / 2.
                    prihdr['CRVAL1'] = 0.
                    prihdr['CRVAL2'] = 0.
                    prihdr['CUNIT1'] = 'DEG'
                    prihdr['CUNIT2'] = 'DEG'
                    prihdr['CD1_1'] = -photom.spatial_sampl.value / 3600.
                    prihdr['CD1_2'] = 0.
                    prihdr['CD2_1'] = 0.
                    prihdr['CD2_2'] = photom.spatial_sampl.value / 3600.
                    prihdr['CTYPE1'] = 'RA---TAN'
                    prihdr['CTYPE2'] = 'DEC--TAN'
                    prihdr['RA'] = 0.
                    prihdr['DEC'] = 0.
                    prihdr['RADECSYS'] = 'FK5'
                    prihdr['EQUINOX'] = 2000.

                    hdu_noise = fits.PrimaryHDU(flux_map_noise)
                    hdulist_noise = fits.HDUList([hdu_noise])
                    prihdr_noise = hdu_noise.header

                    prihdr_noise['BITPIX'] = -32
                    prihdr_noise['NAXIS'] = 2
                    prihdr_noise['NAXIS1'] = photom.nx
                    prihdr_noise['NAXIS2'] = photom.ny
                    prihdr_noise['BSCALE'] = 1.0
                    prihdr_noise['BZERO'] = 0
                    prihdr_noise['BUNIT'] = 'E-16 ERG.S^-1.CM^-2'
                    prihdr_noise['CDELT1'] = -photom.spatial_sampl.value / 3600.
                    prihdr_noise['CDELT2'] = photom.spatial_sampl.value / 3600.
                    prihdr_noise['CRPIX1'] = (photom.nx - 1) / 2.
                    prihdr_noise['CRPIX2'] = (photom.ny - 1) / 2.
                    prihdr_noise['CRVAL1'] = 0.
                    prihdr_noise['CRVAL2'] = 0.
                    prihdr_noise['CUNIT1'] = 'DEG'
                    prihdr_noise['CUNIT2'] = 'DEG'
                    prihdr_noise['CD1_1'] = -photom.spatial_sampl.value / 3600.
                    prihdr_noise['CD1_2'] = 0.
                    prihdr_noise['CD2_1'] = 0.
                    prihdr_noise['CD2_2'] = photom.spatial_sampl.value / 3600.
                    prihdr_noise['CTYPE1'] = 'RA---TAN'
                    prihdr_noise['CTYPE2'] = 'DEC--TAN'
                    prihdr_noise['RA'] = 0.
                    prihdr_noise['DEC'] = 0.
                    prihdr_noise['RADECSYS'] = 'FK5'
                    prihdr_noise['EQUINOX'] = 2000.

                    ext = '_flux'
                    if(photom.dust_to_gas > 0):
                        ext = ext + '_dust'
                    if(mag_ab_limit > -999):
                        ext = ext + '_noise'
                    ext = ext + '_map.fits'

                    hdulist.writeto(
                        photom_dir + '/' + geom.output_dir + '/' + photom_prefix + ext, clobber=True)
                else:
                    print '// ' + output_name + ' already exists [overwrite=False]'

            if(geom.los[ilos] == 'x'):
                data_star["pos"] = np.roll(data_star["pos"], -2, axis=1)
                data_star["vel"] = np.roll(data_star["vel"], -2, axis=1)
                if(photom.dust_to_gas > 0):
                    data_gas["pos"] = np.roll(data_gas["pos"], -2, axis=1)
                    data_gas["vel"] = np.roll(data_gas["vel"], -2, axis=1)
            if(geom.los[ilos] == 'y'):
                data_star["pos"] = np.roll(data_star["pos"], -1, axis=1)
                data_star["vel"] = np.roll(data_star["vel"], -1, axis=1)
                if(photom.dust_to_gas > 0):
                    data_gas["pos"] = np.roll(data_gas["pos"], -1, axis=1)
                    data_gas["vel"] = np.roll(data_gas["vel"], -1, axis=1)

            geom.output_dir = ''


def spectrom_mock(ConfigFile):
    __aurora_version()
    print '////// Aurora - 3D Spectrometry mock module'

    geom = geometry_obj()
    if(not os.path.isfile(ConfigFile)):
        print '// ' + ConfigFile + ' not found'
        return
    geom.parse_input(ConfigFile)

    run = run_obj()
    run.parse_input(ConfigFile)

    spectrom = spectrom_obj()
    spectrom.parse_input(ConfigFile)
    # Defining physical constants
    aconst = constants_obj()
    for ifile in range(len(run.input_file)):
        # Reading simulation input file
        try:
            print "// Pynbody -> Reading file " + run.input_file[ifile]
            data = pynbody.load(run.input_file[ifile])
        except IOError:
            print '// The input file specified cannot be read'
            return

        # Set a coherent unit system
        data.physical_units(velocity='cm s**-1',
                            distance='kpc', mass='1.99e+43 g')
        # Re-centering
        # Grid code case - recenter on the simulation box center
        try:
            data['pos']-=data.properties['boxsize']/2.0
        except:
            pass
        if(geom.barycenter):
            print '// Centering'
            temp = data.gas['mass']
            del temp
            pynbody.analysis.halo.center(data)
            print '// Aligning the disk'
            pynbody.analysis.angmom.faceon(data)
        else:
            data["x"] -= geom.centerx
            data["y"] -= geom.centery
            data["z"] -= geom.centerz

        if((geom.phi != 0)):
            data.rotate_y(geom.phi.value)
            print '// Rotating along y ' + str(geom.phi).strip()
        if((geom.theta != 0)):
            data.rotate_z(geom.theta.value)
            print '// Rotating along z ' + str(geom.theta).strip()

        # Applying coordinates filter
        ok = np.where((data["x"] >= geom.amin.value) & (data["x"] <= geom.amax.value) & (data["y"] >= geom.bmin.value) & (
            data["y"] <= geom.bmax.value) & (data["z"] >= geom.cmin.value) & (data["z"] <= geom.cmax.value))
        data = data[ok]
        del ok

        # Computing cosmological luminous distance
        geom.dl = cosmo.luminosity_distance(geom.redshift)
        # spectrom_pixsize is the size of the pixel in kpc
        if(geom.pixsize <= 0.0):
            geom.pixsize = spectrom.spatial_sampl * \
                (cosmo.kpc_proper_per_arcmin(geom.redshift).to('pc / arcsec'))
        else:
            spectrom.spatial_sampl = (
                geom.pixsize / (cosmo.kpc_proper_per_arcmin(geom.redshift).to('pc / arcsec')))
        print "// Pixel size -> " + str(geom.pixsize).strip()
        print("// Cosmological Luminous distance -> %10.2f Mpc" %
              geom.dl.to('Mpc').value)

        if(len(data.g) > 0):
            if((len(np.unique(data.gas['smooth'])) > 1) & (len(np.unique(data.gas['smooth'])) < 20)):
                run.fft_hsml_limits = np.sort(np.unique(data.gas['smooth']).in_units('kpc'))
                run.nfft = len(run.fft_hsml_limits)
		run.fft_hsml_min = np.min(data.gas['smooth'].in_units('pc'))*unit.pc
        	print '// smoothing groups',run.fft_hsml_min
            else:
                run.fft_hsml_limits = np.arange(1.0, run.nfft + 1)
                run.fft_hsml_limits *= run.fft_hsml_min.to('kpc')
        else:
            print 'No gas elements in this snapshot'
            sys.exit()
        print '// smoothing groups',run.fft_hsml_min
        print '// ' + str(run.nfft).strip() + ' levels for adaptive smoothing'

        data_gas = data.gas
        data_star = data.star
        del data
        gc.collect()

        if(run.star_minmax_keys != ''):
            for i in range(len(run.star_minmax_keys)):
                print '// Restraining data to {0:.2e}<{1}<{2:.2e}'.format(run.star_min_values[i], run.star_minmax_keys[i], run.star_max_values[i])
                print '// Min=', np.min(data_stars[run.star_minmax_keys[i]]), 'Max=', np.max(data_stars[run.star_minmax_keys[i]])
                ok = np.where((data_star[run.stars_minmax_keys[i]] > run.star_min_values[i]) & (
                    data_stars[run.star_minmax_keys[i]] < run.star_max_values[i]))
                if(ok[0].size == 0):
                    print '// No data within specified boundaries'
                    sys.exit()
                data_star = data_star[ok[0]]
            print '// ', len(ok[0]), ' elements selected'
            del ok

        if(run.gas_minmax_keys != ''):
            for i in range(len(run.gas_minmax_keys)):
                print '// Restraining gas data to {0:.2e}<{1}<{2:.2e}'.format(run.gas_min_values[i], run.gas_minmax_keys[i], run.gas_max_values[i])
                ok = np.where((data_gas[run.gas_minmax_keys[i]] > run.gas_min_values[i]) & (
                    data_gas[run.gas_minmax_keys[i]] < run.gas_max_values[i]))
                if(ok[0].size == 0):
                    print '// No data within specified boundaries'
                    sys.exit()
                data_gas = data_gas[ok[0]]
            print '// ', len(ok[0]), ' elements selected'
            del ok

        # Sorting particles by age
        age_sorted = np.argsort(data_star["age"])
        data_star = data_star[age_sorted]
        nstars = len(data_star)
        ngas = len(data_gas)
        print '// nstars   -> ', nstars
        print '// ngas     -> ', ngas

        data_gas['pos'] += 0.1*np.random.randn(np.array(data_gas['pos']).shape[0],np.array(data_gas['pos']).shape[1])*np.transpose(np.tile(data_gas['smooth'],(3,1)))
        # Reading SED model file
        try:
            sed_model_tab = np.loadtxt(spectrom.sed_file)
            sed = True
        except:
            sed = False

        for ilos in range(len(geom.los)):
            print '// LOS = ' + geom.los[ilos]
            # LOS is always Z axis
            if((geom.los[ilos] != 'x') & (geom.los[ilos] != 'y') & (geom.los[ilos] != 'z')):
                print '// ' + geom.los[ilos] + ' is not a valid LOS'
                sys.exit()

            if(geom.los[ilos] == 'x'):
                data_star["pos"] = np.roll(data_star["pos"], 2, axis=1)
                data_star["vel"] = np.roll(data_star["vel"], 2, axis=1)
                data_gas["pos"] = np.roll(data_gas["pos"], 2, axis=1)
                data_gas["vel"] = np.roll(data_gas["vel"], 2, axis=1)
            if(geom.los[ilos] == 'y'):
                data_star["pos"] = np.roll(data_star["pos"], 1, axis=1)
                data_star["vel"] = np.roll(data_star["vel"], 1, axis=1)
                data_gas["pos"] = np.roll(data_gas["pos"], 1, axis=1)
                data_gas["vel"] = np.roll(data_gas["vel"], 1, axis=1)

            pargs = spectrom_args()

            if(spectrom.dust_to_gas > 0):
                ok_gas = np.where((data_gas['x'] > np.min(data_star['x'] - np.max(data_gas['smooth'])))
                                  & (data_gas['x'] < np.max(data_star['x'] + np.max(data_gas['smooth'])))
                                  & (data_gas['y'] > np.min(data_star['y'] - np.max(data_gas['smooth'])))
                                  & (data_gas['y'] < np.max(data_star['y'] + np.max(data_gas['smooth'])))
                                  & (data_gas['z'] > np.min(data_star['z'] - np.max(data_gas['smooth']))))
                data_gas = data_gas[ok_gas]

                tree_gas = []
                pargs.data_gas_x = []
                pargs.data_gas_y = []
                pargs.data_gas_z = []
                pargs.data_gas_smooth = []
                pargs.data_gas_metal = []
                pargs.data_gas_rho = []
                pargs.data_gas_mass = []

                for i in range(run.nfft):
                    if(i == 0):
                        ok_level = np.where(
                            (data_gas['smooth'] < 1.1 * run.fft_hsml_limits[i]))
                    elif(i == run.nfft - 1):
                        ok_level = np.where(
                            (data_gas['smooth'] > 1.1 * run.fft_hsml_limits[i - 1]))
                    else:
                        ok_level = np.where((data_gas['smooth'] < 1.1 * run.fft_hsml_limits[i]) & (
                            data_gas['smooth'] > 1.1 * run.fft_hsml_limits[i - 1]))
                    p = zip(np.array(data_gas['x'])[
                            ok_level], np.array(data_gas['y'])[ok_level])
                    if(len(p) > 0):
                        tree_gas.append(KDTree(p))
                        print '// Building Tree Level', i
                        # Store gas particles
                        pargs.data_gas_x.append(
                            np.array(data_gas['x'])[ok_level])
                        pargs.data_gas_y.append(
                            np.array(data_gas['y'])[ok_level])
                        pargs.data_gas_z.append(
                            np.array(data_gas['z'])[ok_level])
                        pargs.data_gas_smooth.append(
                            np.array(data_gas['smooth'])[ok_level])
                        pargs.data_gas_metal.append(
                            np.array(data_gas['metal'])[ok_level])
                        pargs.data_gas_rho.append(
                            np.array(data_gas['rho'].in_units('2.200789e-24 g cm^-3'))[ok_level])
                        pargs.data_gas_mass.append(
                            np.array(data_gas['mass'])[ok_level])

                # Store gas particles tree
                pargs.tree_gas = tree_gas

            # Set output name
            spectrom_dir = run.input_file[ifile].split(
                '.')[0] + '_spectrom_nx' + str(spectrom.spatial_dim) + 'ny' + str(spectrom.spatial_dim)
            if(geom.output_dir == ''):
                if(geom.redshift < 0.01):
                    geom.output_dir = 'z' + str(int(geom.redshift * 1000) / 1000.) + '_theta' + str(
                        int(geom.theta.value)) + '_phi' + str(int(geom.phi.value)) + '_los' + geom.los[ilos]
                else:
                    geom.output_dir = 'z' + str(int(geom.redshift * 100) / 100.) + '_theta' + str(
                        int(geom.theta.value)) + '_phi' + str(int(geom.phi.value)) + '_los' + geom.los[ilos]

            spectrom_prefix = 'Ha'
            ext = '_flux'
            if(spectrom.dust_to_gas > 0):
                ext = ext + '_dust'
            if(spectrom.sigma_cont > 0):
                ext = ext + '_noise'
            ext = ext + '_cube.fits'
            output_name = spectrom_dir + '/' + geom.output_dir + '/' + spectrom_prefix + ext
            # Check if output already exists
            if((not os.path.isfile(output_name))or(run.overwrite)):
                Halpha_em = (1.0 + geom.redshift) * aconst.Halpha0
                print "// Halpha shifted to " + str(Halpha_em.to('micron').value).strip() + " microns"
                wavelength = np.arange(spectrom.spectral_dim)
                wavelength = (wavelength - spectrom.spectral_dim / 2.) * \
                    spectrom.spectral_sampl.to(
                        'angstrom').value + Halpha_em.to('angstrom').value
                if(sed):
                    print '// SED-model file loaded [' + spectrom.sed_file + ']'
                    # Reading filter transmission file
                    spectrom.filter_lambda = wavelength
                    spectrom.filter_trans = np.tile(1.0, len(wavelength))
                    spectral_domain = np.where((sed_model_tab[:, 1] * (1 + geom.redshift) >= 0.975 * np.min(spectrom.filter_lambda)) & (
                        sed_model_tab[:, 1] * (1 + geom.redshift) <= 1.025 * np.max(spectrom.filter_lambda)))
                    sed_model_tab_filter = sed_model_tab[spectral_domain]
                    # SED time expressed in Myr
                    spectrom.sed_model_time = sed_model_tab_filter[
                        :, 0] / 1.0E6
                    first_timestep = np.where(
                        spectrom.sed_model_time == spectrom.sed_model_time[0])
                    # SED wavelength expressed in cm
                    spectrom.sed_model_lambda = sed_model_tab_filter[:, 1]
                    # Interpolating the filter transmission to the SED model
                    # wavelengths
                    spectrom.filter_trans = np.interp(spectrom.sed_model_lambda[
                                                      first_timestep], spectrom.filter_lambda, spectrom.filter_trans)
                    spectrom.filter_lambda = spectrom.sed_model_lambda[
                        first_timestep]

                    # SED luminosity expressed in erg.s^-1.microns^-1 (expressed in erg.s^-1.angstrom^-1 in the SED file)
                    # We also apply the flux conservation due to redshifting
                    spectrom.sed_model_flux = np.power(10.0, sed_model_tab_filter[
                                                       :, 2]) + np.power(10.0, sed_model_tab_filter[:, 3])
                    del sed_model_tab_filter
                    # Computing stellar extinction in rest-frame
                    sed_model_lambda_micron = spectrom.sed_model_lambda * 1.0e-4

                    sed_vis = np.where((sed_model_lambda_micron >= 0.12) & (
                        sed_model_lambda_micron <= 0.63))
                    sed_ir = np.where((sed_model_lambda_micron >= 0.63) & (
                        sed_model_lambda_micron <= 2.20))
                    spectrom.sed_obscuration = np.zeros(
                        len(spectrom.sed_model_lambda))

                    if(sed_vis[0].size > 0):
                        spectrom.sed_obscuration[
                            sed_vis] = 2.659 * (-1.857 + 1.040 / sed_model_lambda_micron[sed_vis]) + spectrom.rv
                    if(sed_ir[0].size > 0):
                        spectrom.sed_obscuration[sed_ir] = 2.659 * (-2.156 + 1.509 / sed_model_lambda_micron[sed_ir] - 0.198 / np.power(
                            sed_model_lambda_micron[sed_ir], 2.0) + 0.011 / np.power(sed_model_lambda_micron[sed_ir], 3.0)) + spectrom.rv

                    del sed_model_lambda_micron, sed_vis, sed_ir

                print '// Dust to gas ratio -> ', spectrom.dust_to_gas, ' mag/cm^2'

                nchunk = int(math.ceil(ngas / float(run.nvector)))
                cube = np.zeros(
                    (spectrom.spectral_dim, spectrom.spatial_dim, spectrom.spatial_dim, run.nfft))
                if(run.ncpu > 1):
                    pool = mp.Pool(processes=run.ncpu)
                print '// ', run.ncpu, ' threads'
                update_progress(0.0)
                sys.stdout.flush()
                n = 0

                if(run.ncpu > 1):
                    results = []
                    for i in range(nchunk):
                        start = i * run.nvector
                        stop = start + min(run.nvector, ngas - start)
                        pargs.data_gas1_x = np.array(data_gas['x'][start:stop])
                        pargs.data_gas1_y = np.array(data_gas['y'][start:stop])
                        pargs.data_gas1_z = np.array(data_gas['z'][start:stop])
                        pargs.data_gas1_vz = np.array(
                            data_gas['vz'][start:stop].in_units('cm s**-1'))
                        # For some weird reason, the mass and age arrays need
                        # to be accessed to return the right values
                        pargs.data_gas1_dens = np.array(
                            data_gas['rho'][start:stop].in_units('g cm**-3'))
                        pargs.data_gas1_metal = np.array(
                            data_gas['metal'][start:stop])
                        pargs.data_gas1_temp = np.array(
                            data_gas['temp'][start:stop])
                        pargs.data_gas1_smooth = np.array(
                            data_gas['smooth'][start:stop].in_units('kpc'))
                        try:
                            pargs.data_gas1_HII = np.array(
                                data_gas['HII'][start:stop])
                        except:
                            pargs.data_gas1_HII = np.zeros(stop - start)
                            ok = np.where(pargs.data_gas1_temp >
                                          spectrom.T_HII)
                            pargs.data_gas1_HII[:] = 1.0
                        results.append(pool.apply_async(
                            __project_spectrom_flux, [geom, spectrom, run, pargs]))
                        update_progress(float(n) / nchunk)
                        sys.stdout.flush()
                        # Check completion of previous threads
                        to_del = []
                        for k in range(len(results) - 1, -1, -1):
                            if(results[k].ready()):
                                cube += results[k].get()
                                to_del.append(k)
                                n = n + 1
                        # Delete completed threads
                        if(len(to_del) > 0):
                            for k in to_del:
                                del results[k]
                                gc.collect()

                else:
                    ncells = 0
                    for i in range(nchunk):
                        start = i * run.nvector
                        stop = start + min(run.nvector, ngas - start)
                        pargs.data_gas1_x = np.array(data_gas['x'][start:stop])
                        pargs.data_gas1_y = np.array(data_gas['y'][start:stop])
                        pargs.data_gas1_z = np.array(data_gas['z'][start:stop])
                        pargs.data_gas1_vz = np.array(
                            data_gas['vz'][start:stop].in_units('cm s**-1'))
                        # For some weird reason, the mass and age arrays need
                        # to be accessed to return the right values
                        pargs.data_gas1_dens = np.array(
                            data_gas['rho'][start:stop].in_units('g cm**-3'))
                        pargs.data_gas1_metal = np.array(
                            data_gas['metal'][start:stop])
                        pargs.data_gas1_temp = np.array(
                            data_gas['temp'][start:stop])
                        pargs.data_gas1_smooth = np.array(
                            data_gas['smooth'][start:stop].in_units('kpc'))
                        try:
                            pargs.data_gas1_HII = np.array(
                                data_gas['HII'][start:stop])
                        except:
                            pargs.data_gas1_HII = np.zeros(stop - start)
                            ok = np.where(pargs.data_gas1_temp >
                                          spectrom.T_HII)
                            pargs.data_gas1_HII[ok] = 1.0
                        cube += __project_spectrom_flux(geom,
                                                        spectrom, run, pargs)
                        update_progress(float(i + 1) / nchunk)
                        sys.stdout.flush()
                if(run.ncpu > 1):
                    while True:
                        n = sum(1 for x in results if not x.ready())
                        update_progress(float(nchunk - n) / nchunk)
                        sys.stdout.flush()
                        if(n == 0):
                            break

                    for x in results:
                        cube += x.get()

                    pool.close()
                    pool.join()
                    del results
                    gc.collect()

                print '// Total flux -> ' + str(np.sum(cube) * spectrom.spectral_sampl.to('angstrom').value * 1e-4) + ' E-16 erg.s^-1.cm^-2'
                # Spatial smoothing
                print "// Cube adaptive smoothing"
                __cube_convolution(
                    cube, run.nfft, run.fft_hsml_limits, geom.pixsize, run.kernel_scale)
                cube = np.sum(cube, axis=3)

                # Spectral smoothing
                if(spectrom.spectral_res > 0):
                    print '// Spectral smoothing'
                    psf_fwhm = Halpha_em.to(
                        'angstrom').value / (spectrom.spectral_res * spectrom.spectral_sampl.to('angstrom').value)
                    psf_sigma = psf_fwhm / \
                        (2.0 * math.sqrt(2.0 * math.log(2.0)))
                    extended_borders = math.ceil(spectrom.spectral_dim * 2)
                    if(psf_sigma >= 1.0):
                        # We extend the spatial dimensions with 2 times the
                        # seeing to be sure to avoid edge issues.
                        sizez_edge = spectrom.spectral_dim + 2 * extended_borders
                        extended_cube = np.zeros(
                            (sizez_edge, spectrom.spatial_dim, spectrom.spatial_dim))
                        extended_cube[extended_borders:sizez_edge -
                                      extended_borders, :, :] = cube
                        psf = np.exp(-0.5 * np.power((np.arange(sizez_edge) - sizez_edge /
                                                      2.) / psf_sigma, 2)) / (psf_sigma * math.sqrt(2.0 * math.pi))
                        psf = np.roll(psf, int(sizez_edge / 2), axis=0)
                        fft_psf = np.fft.fft(psf)
                        for i in range(spectrom.spatial_dim):
                            for j in range(spectrom.spatial_dim):
                                cube[:, i, j] = (np.fft.ifft(fft_psf * np.fft.fft(extended_cube[:, i, j]))).real[
                                    extended_borders:sizez_edge - extended_borders]

                # Spatial smoothing
                if(spectrom.spatial_res > 0):
                    print '// Spatial smoothing'
                    psf_fwhm = spectrom.spatial_res.value / spectrom.spatial_sampl.value
                    psf_sigma = psf_fwhm / \
                        (2.0 * math.sqrt(2.0 * math.log(2.0)))
                    # We extend the spatial dimensions with 2 times the seeing
                    # to be sure to avoid edge issues.
                    extended_borders = math.ceil(2. * psf_fwhm)
                    sizex_edge = spectrom.spatial_dim + 2 * extended_borders
                    sizey_edge = spectrom.spatial_dim + 2 * extended_borders
                    extended_cube = np.zeros(
                        (spectrom.spectral_dim, sizex_edge, sizey_edge))
                    extended_cube[:, extended_borders:sizex_edge - extended_borders,
                                  extended_borders:sizey_edge - extended_borders] = cube
                    # Create a 2D Gaussian modelling the PSF, centrered in the
                    # middle of the array
                    psfx = np.exp(-0.5 * np.power((np.arange(sizex_edge) - sizex_edge /
                                                   2.) / psf_sigma, 2)) / (psf_sigma * math.sqrt(2.0 * math.pi))
                    psfy = np.exp(-0.5 * np.power((np.arange(sizey_edge) - sizey_edge /
                                                   2.) / psf_sigma, 2)) / (psf_sigma * math.sqrt(2.0 * math.pi))
                    psf = np.tile(psfx, (sizex_edge, 1)) * \
                        np.transpose(np.tile(psfy, (sizey_edge, 1)))

                    psf = np.roll(psf, int(sizex_edge / 2), axis=0)
                    psf = np.roll(psf, int(sizex_edge / 2), axis=1)
                    fft_psf = np.fft.fft2(psf)
                    for i in range(spectrom.spectral_dim):
                        cube[i, :, :] = (np.fft.ifft2(fft_psf * np.fft.fft2(extended_cube[i, :, :]))).real[
                            extended_borders:sizex_edge - extended_borders, extended_borders:sizey_edge - extended_borders]

                # Noise injection
                if(spectrom.sigma_cont > 0.):
                    print '// Noise injection'
                    cube_noise = cube + np.random.normal(
                        0.0, spectrom.sigma_cont, (spectrom.spectral_dim, spectrom.spatial_dim, spectrom.spatial_dim))
                try:
                    os.mkdir(spectrom_dir)
                except:
                    print '// ' + spectrom_dir + ' exist'

                try:
                    os.mkdir(spectrom_dir + '/' + geom.output_dir)
                except:
                    print '// ' + spectrom_dir + '/' + geom.output_dir + ' exists'

                # Writing maps to FITS files
                hdu = fits.PrimaryHDU(cube)
                hdulist = fits.HDUList([hdu])
                prihdr = hdu.header
                # Filling FITS header
                prihdr['BITPIX'] = -32
                prihdr['NAXIS'] = 3
                prihdr['NAXIS1'] = spectrom.spatial_dim
                prihdr['NAXIS2'] = spectrom.spatial_dim
                prihdr['NAXIS3'] = spectrom.spectral_dim
                prihdr['BSCALE'] = 1.0
                prihdr['BZERO'] = 0
                prihdr['BUNIT'] = 'E-16 ERG.S^-1.CM^-2.MICRONS^-1'
                prihdr['CRPIX3'] = int((spectrom.spectral_dim - 1) / 2.)
                prihdr['CRVAL3'] = Halpha_em.to('micron').value
                prihdr['CDELT3'] = spectrom.spectral_sampl.to('micron').value
                prihdr['CTYPE3'] = 'WAVELENGTH'
                prihdr['CUNIT3'] = 'MICRONS'
                prihdr['CDELT1'] = -spectrom.spatial_sampl.value / 3600.
                prihdr['CDELT2'] = spectrom.spatial_sampl.value / 3600.
                prihdr['CRPIX1'] = (spectrom.spatial_dim - 1) / 2.
                prihdr['CRPIX2'] = (spectrom.spatial_dim - 1) / 2.
                prihdr['CRVAL1'] = 0.
                prihdr['CRVAL2'] = 0.
                prihdr['CUNIT1'] = 'DEG'
                prihdr['CUNIT2'] = 'DEG'
                prihdr['CD1_1'] = -spectrom.spatial_sampl.to('deg').value
                prihdr['CD1_2'] = 0.
                prihdr['CD2_1'] = 0.
                prihdr['CD2_2'] = spectrom.spatial_sampl.to('deg').value
                prihdr['CTYPE1'] = 'RA---TAN'
                prihdr['CTYPE2'] = 'DEC--TAN'
                prihdr['RA'] = 0.
                prihdr['DEC'] = 0.
                prihdr['RADECSYS'] = 'FK5'
                prihdr['EQUINOX'] = 2000.
                prihdr['fp_h_co'] = 0.
                prihdr['fp_b_l'] = Halpha_em.to(
                    'angstrom').value - spectrom.spectral_sampl.to('angstrom').value * spectrom.spectral_dim / 2.
                prihdr['fp_i_a'] = spectrom.spectral_dim * \
                    spectrom.spectral_sampl.to('angstrom').value
                prihdr['fp_l_re'] = aconst.Halpha0.to('angstrom').value
                prihdr['fp_l_ref'] = aconst.Halpha0.to('angstrom').value

                if(spectrom.sigma_cont > 0):
                    hdu_noise = fits.PrimaryHDU(cube_noise)
                    hdulist_noise = fits.HDUList([hdu_noise])
                    prihdr_noise = hdu_noise.header
                    # Filling FITS header
                    prihdr_noise['BITPIX'] = -32
                    prihdr_noise['NAXIS'] = 3
                    prihdr_noise['NAXIS1'] = spectrom.spatial_dim
                    prihdr_noise['NAXIS2'] = spectrom.spatial_dim
                    prihdr_noise['NAXIS3'] = spectrom.spectral_dim
                    prihdr_noise['BSCALE'] = 1.0
                    prihdr_noise['BZERO'] = 0
                    prihdr_noise['BUNIT'] = 'E-16 ERG.S^-1.CM^-2.MICRONS^-1'
                    prihdr_noise['CRPIX3'] = int(
                        (spectrom.spectral_dim - 1) / 2.)
                    prihdr_noise['CRVAL3'] = Halpha_em * 1e4
                    prihdr_noise[
                        'CDELT3'] = spectrom.spectral_sampl.to('micron')
                    prihdr_noise['CTYPE3'] = 'WAVELENGTH'
                    prihdr_noise['CUNIT3'] = 'MICRONS'
                    prihdr_noise['CDELT1'] = - \
                        spectrom.spatial_sampl.to('deg').value
                    prihdr_noise['CDELT2'] = spectrom.spatial_sampl.to(
                        'deg').value
                    prihdr_noise['CRPIX1'] = (spectrom.spatial_dim - 1) / 2.
                    prihdr_noise['CRPIX2'] = (spectrom.spatial_dim - 1) / 2.
                    prihdr_noise['CRVAL2'] = 0.
                    prihdr_noise['CUNIT1'] = 'DEG'
                    prihdr_noise['CUNIT2'] = 'DEG'
                    prihdr_noise['CD1_1'] = - \
                        spectrom.spatial_sampl.to('deg').value
                    prihdr_noise['CD1_2'] = 0.
                    prihdr_noise['CD2_1'] = 0.
                    prihdr_noise['CD2_2'] = spectrom.spatial_sampl.to(
                        'deg').value
                    prihdr_noise['CTYPE1'] = 'RA---TAN'
                    prihdr_noise['CTYPE2'] = 'DEC--TAN'
                    prihdr_noise['RA'] = 0.
                    prihdr_noise['DEC'] = 0.
                    prihdr_noise['RADECSYS'] = 'FK5'
                    prihdr_noise['EQUINOX'] = 2000.
                    prihdr_noise['fp_h_co'] = 0.
                    prihdr_noise['fp_b_l'] = Halpha_em.to(
                        'angstrom').value - spectrom.spectral_sampl.to('angstrom').value * spectrom.spectral_dim / 2.
                    prihdr_noise['fp_i_a'] = spectrom.spectral_dim * \
                        spectrom.spectral_sampl.to('angstrom').value
                    prihdr_noise['fp_l_re'] = aconst.Halpha0.to(
                        'angstrom').value
                    prihdr_noise['fp_l_ref'] = aconst.Halpha0.to(
                        'angstrom').value

                ext = '_flux'
                if(spectrom.dust_to_gas > 0):
                    ext = ext + '_dust'
                if(spectrom.sigma_cont > 0):
                    ext = ext + '_noise'
                ext = ext + '_map.fits'

                hdulist.writeto(spectrom_dir + '/' + geom.output_dir +
                                '/' + spectrom_prefix + ext, clobber=True)
            else:
                print '// ' + output_name + ' already exists [overwrite=False]'

            if(geom.los[ilos] == 'x'):
                data_star["pos"] = np.roll(data_star["pos"], -2, axis=1)
                data_star["vel"] = np.roll(data_star["vel"], -2, axis=1)
                data_gas["pos"] = np.roll(data_gas["pos"], -2, axis=1)
                data_gas["vel"] = np.roll(data_gas["vel"], -2, axis=1)
            if(geom.los[ilos] == 'y'):
                data_star["pos"] = np.roll(data_star["pos"], -1, axis=1)
                data_star["vel"] = np.roll(data_star["vel"], -1, axis=1)
                data_gas["pos"] = np.roll(data_gas["pos"], -1, axis=1)
                data_gas["vel"] = np.roll(data_gas["vel"], -1, axis=1)

            geom.output_dir = ''
